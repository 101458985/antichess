# AntiChess Project #
---

### This project ###
---
+  This repository serves as a version control for my AntiChess project for my 2017 COS20007 D/HD Custom Program at Swinburne University.
+  This project is to demonstrate my grasp of Object Oriented principles, as well as my skills to design and create a moderately complex program.
+  Read The Docs is linked to this repository to manage and generate project documentation.


### Project Overview ###
---
+  This project is a WinForms program that plays the game of AntiChess.
+  The game is built around a local 2-player competitive match where the same computer is shared by the competitors.
+  This project is only a demonstration of object-oriented principles, as well as a proof-of-concept. It should not be considered or treated as an end product.


### Instructions ###
---
View the game instructions [here](HowToPlay.md).


### Design ###
---
+  This program was designed and implemented under object-oriented principles in the C# language
+  Instances of Abstraction, Encapsulation, Polymorphism and Inheritance are all present within the code.


+  The solution was designed with OOP best principles, which feature low coupling and high cohesion.
+  All methods within an object have clear purposes and perform meaningful actions. These methods are often self reliant, or use no more than 2 abstract methods within the same class.
+  Objects within the solution are well defined and appropriately encapsulated. Each object only knows and does things that are semantically relevant to itself.

![Oops! Something went wrong!](images/classdiagram.png)

### Known Issues ###
---
When a pawn moves to the opposite end of the board, Black Pawns will transform into a Black King immediately. However, White Pawns will only transform into a White King at the start of White's next turn.
  
The cause of this issue is unknown as both players go through the same methods to process the turn. Some observed but yet unsolved inconsistencies include:

+  The "_activePlayer" flag for some reason does not change immediately after a move is made when it is the Black's turn. This is however not the case when it is White's turn.
+  The SwitchPlayer method is run at the end of every turn, but it's effects do not seem to be evident when it is the end of White's turn, and can only be seen when Black makes a selection. This does not happen the other way around.

### Contact ###
---
Jin Hoe Lim
Email: 101458985@student.swin.edu.au