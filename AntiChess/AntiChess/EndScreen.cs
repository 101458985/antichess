﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AntiChess
{
    partial class EndScreen : Form
    {
        public EndScreen(string _winner, string _victoryCondition, Player _p1, Player _p2)
        {
            int _p1Score = 16 - _p1.Score;
            int _p2Score = 16 - _p2.Score;
            InitializeComponent();

            OutputBox.Text = _winner + " wins by " + _victoryCondition + "\r\n\r\n" //Prints who wins and the final scores
                            + "Final Scores:\r\n\rWhite\r\tBlack\r\n" + _p1Score + "\r\t" + _p2Score;
        }

        void ReplayButton_Click(object sender, EventArgs e) //Restarts the game to be played again
        {
            var _chessGame = (GameScreen)Tag;
            _chessGame.Location = new Point(this.Left, this.Top);
            _chessGame.RestartGame(); //Restarts the first form, show it, then hide this one
            _chessGame.Show();
            Hide();
        }

        void EndScreen_FormClosed(object sender, FormClosedEventArgs e) //Closes the application, not just the form, when X is clicked.
        {
            Application.Exit();
        }
    }
}
