﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AntiChess
{
    class Rook : Piece
    {
        public Rook(bool colour, Image icon) : base(colour, "Rook", icon)
        {
        }

        public override bool CanCapture(Tile[,] _chess, int x, int y)
        {
            bool _canCapture = false;
            
            for (int i = x; i >= 0; i--) //Checks left of Rook
            {
                if (_chess[i, y].GetPiece != null && _chess[i, y].GetPiece.Colour != Colour) //If it is the opposite colour of the current piece
                {
                    _canCapture = true;
                    break;
                }
                else if (_chess[i, y].GetPiece != null && _chess[i, y].GetPiece.Colour == Colour && _chess[i, y].GetPiece != this)
                    break;
            }

            for (int i = x; i <= 7; i++) //Checks right of Rook
            {
                if (_chess[i, y].GetPiece != null && _chess[i, y].GetPiece.Colour != Colour)
                {
                    _canCapture = true;
                    break;
                }
                else if (_chess[i, y].GetPiece != null && _chess[i, y].GetPiece.Colour == Colour && _chess[i, y].GetPiece != this)
                    break;
            }

            for (int i = y; i >= 0; i--) //Checks upwards of Rook
            {
                if (_chess[x, i].GetPiece != null && _chess[x, i].GetPiece.Colour != Colour)
                {
                    _canCapture = true;
                    break;
                }
                else if (_chess[x, i].GetPiece != null && _chess[x, i].GetPiece.Colour == Colour && _chess[x, i].GetPiece != this)
                    break;
            }

            for (int i = y; i <= 7; i++) //Checks downwards of Rook
            {
                if (_chess[x, i].GetPiece != null && _chess[x, i].GetPiece.Colour != Colour)
                {
                    _canCapture = true;
                    break;
                }
                else if (_chess[x, i].GetPiece != null && _chess[x, i].GetPiece.Colour == Colour && _chess[x, i].GetPiece != this)
                    break;
            }

            return _canCapture;
        }

        public override void HighlightMove(Board _chess, int x, int y)
        {
            for (int i = x; i >= 0; i--) //Highlghts left of Rook
            {
                if (_chess.Map[i, y].GetPiece == null) //If there are no pieces in that tile, then it can be moved to
                {
                    _chess.Map[i, y].Selectable = true;
                }
                else if (_chess.Map[i, y].GetPiece != this)
                    break;
            }

            for (int i = x; i <= 7; i++) //Highlghts right of Rook
            {
                if (_chess.Map[i, y].GetPiece == null)
                {
                    _chess.Map[i, y].Selectable = true;
                }
                else if (_chess.Map[i, y].GetPiece != this)
                    break;
            }

            for (int i = y; i >= 0; i--) //Highlghts upwards of Rook
            {
                if (_chess.Map[x, i].GetPiece == null)
                {
                    _chess.Map[x, i].Selectable = true;
                }
                else if (_chess.Map[x, i].GetPiece != this)
                    break;
            }

            for (int i = y; i <= 7; i++) //Highlghts downwards of Rook
            {
                if (_chess.Map[x, i].GetPiece == null)
                {
                    _chess.Map[x, i].Selectable = true;
                }
                else if (_chess.Map[x, i].GetPiece != this)
                    break;
            }
        }

        public override void HighlightCapture(Board _chess, int x, int y)
        {
            for (int i = x; i >= 0; i--) //Checks left of Rook
            {
                if (_chess.Map[i, y].GetPiece != null && _chess.Map[i, y].GetPiece.Colour != Colour) //If it is the opposite colour of the current piece
                {
                    _chess.Map[i, y].Selectable = true;
                    break;
                }
                else if (_chess.Map[i, y].GetPiece != null && _chess.Map[i, y].GetPiece.Colour == Colour && _chess.Map[i, y].GetPiece != this)
                    break;
            }

            for (int i = x; i <= 7; i++) //Checks right of Rook
            {
                if (_chess.Map[i, y].GetPiece != null && _chess.Map[i, y].GetPiece.Colour != Colour)
                {
                    _chess.Map[i, y].Selectable = true;
                    break;
                }
                else if (_chess.Map[i, y].GetPiece != null && _chess.Map[i, y].GetPiece.Colour == Colour && _chess.Map[i, y].GetPiece != this)
                    break;
            }

            for (int i = y; i >= 0; i--) //Checks upwards of Rook
            {
                if (_chess.Map[x, i].GetPiece != null && _chess.Map[x, i].GetPiece.Colour != Colour)
                {
                    _chess.Map[x, i].Selectable = true;
                    break;
                }
                else if (_chess.Map[x, i].GetPiece != null && _chess.Map[x, i].GetPiece.Colour == Colour && _chess.Map[x, i].GetPiece != this)
                    break;
            }

            for (int i = y; i <= 7; i++) //Checks downwards of Rook
            {
                if (_chess.Map[x, i].GetPiece != null && _chess.Map[x, i].GetPiece.Colour != Colour)
                {
                    _chess.Map[x, i].Selectable = true;
                    break;
                }
                else if (_chess.Map[x, i].GetPiece != null && _chess.Map[x, i].GetPiece.Colour == Colour && _chess.Map[x, i].GetPiece != this)
                    break;
            }
        }

    }
}
