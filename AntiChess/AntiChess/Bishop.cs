﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AntiChess
{
    class Bishop : Piece
    {
        public Bishop(bool colour, Image icon) : base(colour, "Bishop", icon)
        {
        }

        public override bool CanCapture(Tile[,] _chess, int x, int y)
        {
            int _xCount, _yCount;
            bool _canCapture = false;

            _xCount = x;
            _yCount = y;
            while (_xCount >= 0 && _yCount >= 0 && !_canCapture) //Checks upper left path of Bishop
            {
                if (_chess[_xCount, _yCount].GetPiece != null && _chess[_xCount, _yCount].GetPiece.Colour != Colour) //If the tile has a piece that is the opposite colour
                    _canCapture = true; //This flag will end the loop and prevent any following loops from starting
                else if (_chess[_xCount, _yCount].GetPiece != null && _chess[_xCount, _yCount].GetPiece.Colour == Colour && _chess[_xCount, _yCount].GetPiece != this)
                    break;

                _xCount--;
                _yCount--;
            }

            _xCount = x; //X and Y references are reset every path to the location of the piece
            _yCount = y;
            while (_xCount >= 0 && _yCount <= 7 && !_canCapture) //Checks lower left path of Bishop
            {
                if (_chess[_xCount, _yCount].GetPiece != null && _chess[_xCount, _yCount].GetPiece.Colour != Colour)
                    _canCapture = true;
                else if (_chess[_xCount, _yCount].GetPiece != null && _chess[_xCount, _yCount].GetPiece.Colour == Colour && _chess[_xCount, _yCount].GetPiece != this)
                    break;

                _xCount--;
                _yCount++;
            }

            _xCount = x;
            _yCount = y;
            while (_xCount <= 7 && _yCount >= 0 && !_canCapture) //Checks upper right path of Bishop
            {
                if (_chess[_xCount, _yCount].GetPiece != null && _chess[_xCount, _yCount].GetPiece.Colour != Colour)
                    _canCapture = true;
                else if (_chess[_xCount, _yCount].GetPiece != null && _chess[_xCount, _yCount].GetPiece.Colour == Colour && _chess[_xCount, _yCount].GetPiece != this)
                    break;

                _xCount++;
                _yCount--;
            }

            _xCount = x;
            _yCount = y;
            while (_xCount <= 7 && _yCount <= 7 && !_canCapture) //Checks lower right path of Bishop
            {
                if (_chess[_xCount, _yCount].GetPiece != null && _chess[_xCount, _yCount].GetPiece.Colour != Colour)
                    _canCapture = true;
                else if (_chess[_xCount, _yCount].GetPiece != null && _chess[_xCount, _yCount].GetPiece.Colour == Colour && _chess[_xCount, _yCount].GetPiece != this)
                    break;

                _xCount++;
                _yCount++;
            }

            return _canCapture;
        }

        public override void HighlightMove(Board _chess, int x, int y)
        {
            int _xCount, _yCount;

            _xCount = x;
            _yCount = y;
            while (_xCount >= 0 && _yCount >= 0) //Highlights upper left path of Bishop
            {
                if (_chess.Map[_xCount, _yCount].GetPiece == null) //If tile is empty, then it can be moved to
                {
                    _chess.Map[_xCount, _yCount].Selectable = true;
                }
                else if (_chess.Map[_xCount, _yCount].GetPiece != this)  //Otherwise it's not empty, so end the loop
                    break;

                _xCount--;
                _yCount--;
            }

            _xCount = x;
            _yCount = y;
            while (_xCount >= 0 && _yCount <= 7) //Highlights lower left path of Bishop
            {
                if (_chess.Map[_xCount, _yCount].GetPiece == null)
                {
                    _chess.Map[_xCount, _yCount].Selectable = true;
                }
                else if (_chess.Map[_xCount, _yCount].GetPiece != this)
                    break;

                _xCount--;
                _yCount++;
            }

            _xCount = x;
            _yCount = y;
            while (_xCount <= 7 && _yCount >= 0) //Highlights upper right path of Bishop
            {
                if (_chess.Map[_xCount, _yCount].GetPiece == null)
                {
                    _chess.Map[_xCount, _yCount].Selectable = true;
                }
                else if (_chess.Map[_xCount, _yCount].GetPiece != this)
                    break;

                _xCount++;
                _yCount--;
            }

            _xCount = x;
            _yCount = y;
            while (_xCount <= 7 && _yCount <= 7) //Highlights lower right path of Bishop
            {
                if (_chess.Map[_xCount, _yCount].GetPiece == null)
                {
                    _chess.Map[_xCount, _yCount].Selectable = true;
                }
                else if (_chess.Map[_xCount, _yCount].GetPiece != this)
                    break;

                _xCount++;
                _yCount++;
            }
        }

        public override void HighlightCapture(Board _chess, int x, int y)
        {
            int _xCount, _yCount;

            _xCount = x;
            _yCount = y;
            while (_xCount >= 0 && _yCount >= 0) //Highlights pieces on upper left path of Bishop
            {
                if (_chess.Map[_xCount, _yCount].GetPiece != null && _chess.Map[_xCount, _yCount].GetPiece.Colour != Colour) //If the tile has a piece that is the opposite colour
                {
                    _chess.Map[_xCount, _yCount].Selectable = true; //Make it selectable
                    break; //Then break the loop to stop checking past it
                }
                else if (_chess.Map[_xCount, _yCount].GetPiece != null && _chess.Map[_xCount, _yCount].GetPiece.Colour == Colour && _chess.Map[_xCount, _yCount].GetPiece != this)
                    break;

                _xCount--;
                _yCount--;
            }

            _xCount = x;
            _yCount = y;
            while (_xCount >= 0 && _yCount <= 7) //Highlights pieces on lower left path of Bishop
            {
                if (_chess.Map[_xCount, _yCount].GetPiece != null && _chess.Map[_xCount, _yCount].GetPiece.Colour != Colour)
                {
                    _chess.Map[_xCount, _yCount].Selectable = true;
                    break;
                }
                else if (_chess.Map[_xCount, _yCount].GetPiece != null && _chess.Map[_xCount, _yCount].GetPiece.Colour == Colour && _chess.Map[_xCount, _yCount].GetPiece != this)
                    break;

                _xCount--;
                _yCount++;
            }

            _xCount = x;
            _yCount = y;
            while (_xCount <= 7 && _yCount >= 0) //Highlights pieces on upper right path of Bishop
            {
                if (_chess.Map[_xCount, _yCount].GetPiece != null && _chess.Map[_xCount, _yCount].GetPiece.Colour != Colour)
                {
                    _chess.Map[_xCount, _yCount].Selectable = true;
                    break;
                }
                else if (_chess.Map[_xCount, _yCount].GetPiece != null && _chess.Map[_xCount, _yCount].GetPiece.Colour == Colour && _chess.Map[_xCount, _yCount].GetPiece != this)
                    break;

                _xCount++;
                _yCount--;
            }

            _xCount = x;
            _yCount = y;
            while (_xCount <= 7 && _yCount <= 7) //Highlights pieces on lower right path of Bishop
            {
                if (_chess.Map[_xCount, _yCount].GetPiece != null && _chess.Map[_xCount, _yCount].GetPiece.Colour != Colour)
                {
                    _chess.Map[_xCount, _yCount].Selectable = true;
                    break;
                }
                else if (_chess.Map[_xCount, _yCount].GetPiece != null && _chess.Map[_xCount, _yCount].GetPiece.Colour == Colour && _chess.Map[_xCount, _yCount].GetPiece != this)
                    break;

                _xCount++;
                _yCount++;
            }

        }
    }
}
