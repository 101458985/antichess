﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AntiChess
{
    class King : Piece
    {
        public King(bool colour,Image icon) : base(colour, "King", icon)
        {
        }

        public override bool CanCapture(Tile[,] _chess, int x, int y)
        {
            bool _canCapture = false;

            for (int _xPos = x-1; _xPos <= x+1; _xPos++)
            {
                for (int _yPos = y-1; _yPos <= y+1; _yPos++) //Loops through 9 tiles around and including the King
                {
                    if (_xPos >= 0 && _xPos <= 7 && _yPos >= 0 && _yPos <= 7) //If the coordinates are in bounds of the array
                    {
                        if (_chess[_xPos,_yPos].GetPiece != null && _chess[_xPos, _yPos].GetPiece.Colour != Colour) //If the tile has a piece that is the opposite colour
                        {
                            _canCapture = true;
                            break;
                        }
                    }
                }
            }

            return _canCapture;
        }

        public override void HighlightMove(Board _chess, int x, int y)
        {
            for (int _xPos = x - 1; _xPos <= x + 1; _xPos++)
            {
                for (int _yPos = y - 1; _yPos <= y + 1; _yPos++) //Loops through 9 tiles around and including the King
                {
                    if (_xPos >= 0 && _xPos <= 7 && _yPos >= 0 && _yPos <= 7) //If the coordinates are in bounds of the array
                    {
                        if (_chess.Map[_xPos, _yPos].GetPiece == null) //If the tile is empty then it can be moved
                        {
                            _chess.Map[_xPos, _yPos].Selectable = true;
                        }
                    }
                }
            }
        }

        public override void HighlightCapture(Board _chess, int x, int y)
        {
            for (int _xPos = x - 1; _xPos <= x + 1; _xPos++)
            {
                for (int _yPos = y - 1; _yPos <= y + 1; _yPos++) //Loops through 9 tiles around and including the King
                {
                    if (_xPos >= 0 && _xPos <= 7 && _yPos >= 0 && _yPos <= 7) //If the coordinates are in bounds of the array
                    {
                        if (_chess.Map[_xPos, _yPos].GetPiece != null && _chess.Map[_xPos, _yPos].GetPiece.Colour != Colour) //If the tile has a piece that is the opposite colour
                        {
                            _chess.Map[_xPos, _yPos].Selectable = true;
                        }
                    }
                }
            }
        }
    }
}
