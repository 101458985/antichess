﻿namespace AntiChess
{
    partial class GameScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GameScreen));
            this.tile5a = new System.Windows.Forms.Panel();
            this.tile4h = new System.Windows.Forms.Panel();
            this.tile5b = new System.Windows.Forms.Panel();
            this.tile4e = new System.Windows.Forms.Panel();
            this.tile6a = new System.Windows.Forms.Panel();
            this.tile4g = new System.Windows.Forms.Panel();
            this.tile6c = new System.Windows.Forms.Panel();
            this.tile3h = new System.Windows.Forms.Panel();
            this.tile6b = new System.Windows.Forms.Panel();
            this.tile4f = new System.Windows.Forms.Panel();
            this.tile5c = new System.Windows.Forms.Panel();
            this.tile3e = new System.Windows.Forms.Panel();
            this.tile7b = new System.Windows.Forms.Panel();
            this.tile3g = new System.Windows.Forms.Panel();
            this.tile5d = new System.Windows.Forms.Panel();
            this.tile2h = new System.Windows.Forms.Panel();
            this.tile6d = new System.Windows.Forms.Panel();
            this.tile1h = new System.Windows.Forms.Panel();
            this.tile7c = new System.Windows.Forms.Panel();
            this.tile3f = new System.Windows.Forms.Panel();
            this.tile7a = new System.Windows.Forms.Panel();
            this.tile1g = new System.Windows.Forms.Panel();
            this.tile8b = new System.Windows.Forms.Panel();
            this.tile2f = new System.Windows.Forms.Panel();
            this.tile7d = new System.Windows.Forms.Panel();
            this.tile2g = new System.Windows.Forms.Panel();
            this.tile8c = new System.Windows.Forms.Panel();
            this.tile2e = new System.Windows.Forms.Panel();
            this.tile8a = new System.Windows.Forms.Panel();
            this.tile1f = new System.Windows.Forms.Panel();
            this.tile8d = new System.Windows.Forms.Panel();
            this.tile1e = new System.Windows.Forms.Panel();
            this.tile5e = new System.Windows.Forms.Panel();
            this.tile4d = new System.Windows.Forms.Panel();
            this.tile5f = new System.Windows.Forms.Panel();
            this.tile4a = new System.Windows.Forms.Panel();
            this.tile6e = new System.Windows.Forms.Panel();
            this.tile4c = new System.Windows.Forms.Panel();
            this.tile6g = new System.Windows.Forms.Panel();
            this.tile3d = new System.Windows.Forms.Panel();
            this.tile6f = new System.Windows.Forms.Panel();
            this.tile4b = new System.Windows.Forms.Panel();
            this.tile5g = new System.Windows.Forms.Panel();
            this.tile3a = new System.Windows.Forms.Panel();
            this.tile7f = new System.Windows.Forms.Panel();
            this.tile3c = new System.Windows.Forms.Panel();
            this.tile5h = new System.Windows.Forms.Panel();
            this.tile2d = new System.Windows.Forms.Panel();
            this.tile6h = new System.Windows.Forms.Panel();
            this.tile1d = new System.Windows.Forms.Panel();
            this.tile7g = new System.Windows.Forms.Panel();
            this.tile3b = new System.Windows.Forms.Panel();
            this.tile7e = new System.Windows.Forms.Panel();
            this.tile1c = new System.Windows.Forms.Panel();
            this.tile8f = new System.Windows.Forms.Panel();
            this.tile2b = new System.Windows.Forms.Panel();
            this.tile7h = new System.Windows.Forms.Panel();
            this.tile2c = new System.Windows.Forms.Panel();
            this.tile8g = new System.Windows.Forms.Panel();
            this.tile2a = new System.Windows.Forms.Panel();
            this.tile8e = new System.Windows.Forms.Panel();
            this.tile1b = new System.Windows.Forms.Panel();
            this.tile8h = new System.Windows.Forms.Panel();
            this.tile1a = new System.Windows.Forms.Panel();
            this.CancelButton = new System.Windows.Forms.Button();
            this.ConcedeButton = new System.Windows.Forms.Button();
            this.OutputBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.WhiteOdds = new System.Windows.Forms.TextBox();
            this.BlackOdds = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.BlackScore = new System.Windows.Forms.TextBox();
            this.WhiteScore = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tile5a
            // 
            this.tile5a.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.tile5a.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile5a.Location = new System.Drawing.Point(12, 298);
            this.tile5a.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile5a.Name = "tile5a";
            this.tile5a.Size = new System.Drawing.Size(60, 64);
            this.tile5a.TabIndex = 32;
            this.tile5a.Click += new System.EventHandler(this.tile5a_Click);
            // 
            // tile4h
            // 
            this.tile4h.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.tile4h.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile4h.Location = new System.Drawing.Point(474, 226);
            this.tile4h.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile4h.Name = "tile4h";
            this.tile4h.Size = new System.Drawing.Size(60, 64);
            this.tile4h.TabIndex = 31;
            this.tile4h.Click += new System.EventHandler(this.tile4h_Click);
            // 
            // tile5b
            // 
            this.tile5b.BackColor = System.Drawing.Color.Moccasin;
            this.tile5b.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile5b.Location = new System.Drawing.Point(78, 298);
            this.tile5b.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile5b.Name = "tile5b";
            this.tile5b.Size = new System.Drawing.Size(60, 64);
            this.tile5b.TabIndex = 33;
            this.tile5b.Click += new System.EventHandler(this.tile5b_Click);
            // 
            // tile4e
            // 
            this.tile4e.BackColor = System.Drawing.Color.Moccasin;
            this.tile4e.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile4e.Location = new System.Drawing.Point(276, 226);
            this.tile4e.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile4e.Name = "tile4e";
            this.tile4e.Size = new System.Drawing.Size(60, 64);
            this.tile4e.TabIndex = 30;
            this.tile4e.Click += new System.EventHandler(this.tile4e_Click);
            // 
            // tile6a
            // 
            this.tile6a.BackColor = System.Drawing.Color.Moccasin;
            this.tile6a.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile6a.Location = new System.Drawing.Point(12, 368);
            this.tile6a.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile6a.Name = "tile6a";
            this.tile6a.Size = new System.Drawing.Size(60, 64);
            this.tile6a.TabIndex = 34;
            this.tile6a.Click += new System.EventHandler(this.tile6a_Click);
            // 
            // tile4g
            // 
            this.tile4g.BackColor = System.Drawing.Color.Moccasin;
            this.tile4g.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile4g.Location = new System.Drawing.Point(408, 226);
            this.tile4g.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile4g.Name = "tile4g";
            this.tile4g.Size = new System.Drawing.Size(60, 64);
            this.tile4g.TabIndex = 29;
            this.tile4g.Click += new System.EventHandler(this.tile4g_Click);
            // 
            // tile6c
            // 
            this.tile6c.BackColor = System.Drawing.Color.Moccasin;
            this.tile6c.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile6c.Location = new System.Drawing.Point(144, 368);
            this.tile6c.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile6c.Name = "tile6c";
            this.tile6c.Size = new System.Drawing.Size(60, 64);
            this.tile6c.TabIndex = 35;
            this.tile6c.Click += new System.EventHandler(this.tile6c_Click);
            // 
            // tile3h
            // 
            this.tile3h.BackColor = System.Drawing.Color.Moccasin;
            this.tile3h.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile3h.Location = new System.Drawing.Point(474, 155);
            this.tile3h.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile3h.Name = "tile3h";
            this.tile3h.Size = new System.Drawing.Size(60, 64);
            this.tile3h.TabIndex = 28;
            this.tile3h.Click += new System.EventHandler(this.tile3h_Click);
            // 
            // tile6b
            // 
            this.tile6b.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.tile6b.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile6b.Location = new System.Drawing.Point(78, 368);
            this.tile6b.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile6b.Name = "tile6b";
            this.tile6b.Size = new System.Drawing.Size(60, 64);
            this.tile6b.TabIndex = 36;
            this.tile6b.Click += new System.EventHandler(this.tile6b_Click);
            // 
            // tile4f
            // 
            this.tile4f.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.tile4f.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile4f.Location = new System.Drawing.Point(342, 226);
            this.tile4f.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile4f.Name = "tile4f";
            this.tile4f.Size = new System.Drawing.Size(60, 64);
            this.tile4f.TabIndex = 27;
            this.tile4f.Click += new System.EventHandler(this.tile4f_Click);
            // 
            // tile5c
            // 
            this.tile5c.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.tile5c.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile5c.Location = new System.Drawing.Point(144, 298);
            this.tile5c.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile5c.Name = "tile5c";
            this.tile5c.Size = new System.Drawing.Size(60, 64);
            this.tile5c.TabIndex = 37;
            this.tile5c.Click += new System.EventHandler(this.tile5c_Click);
            // 
            // tile3e
            // 
            this.tile3e.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.tile3e.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile3e.Location = new System.Drawing.Point(276, 155);
            this.tile3e.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile3e.Name = "tile3e";
            this.tile3e.Size = new System.Drawing.Size(60, 64);
            this.tile3e.TabIndex = 26;
            this.tile3e.Click += new System.EventHandler(this.tile3e_Click);
            // 
            // tile7b
            // 
            this.tile7b.BackColor = System.Drawing.Color.Moccasin;
            this.tile7b.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile7b.Location = new System.Drawing.Point(78, 439);
            this.tile7b.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile7b.Name = "tile7b";
            this.tile7b.Size = new System.Drawing.Size(60, 64);
            this.tile7b.TabIndex = 38;
            this.tile7b.Click += new System.EventHandler(this.tile7b_Click);
            // 
            // tile3g
            // 
            this.tile3g.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.tile3g.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile3g.Location = new System.Drawing.Point(408, 155);
            this.tile3g.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile3g.Name = "tile3g";
            this.tile3g.Size = new System.Drawing.Size(60, 64);
            this.tile3g.TabIndex = 25;
            this.tile3g.Click += new System.EventHandler(this.tile3g_Click);
            // 
            // tile5d
            // 
            this.tile5d.BackColor = System.Drawing.Color.Moccasin;
            this.tile5d.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile5d.Location = new System.Drawing.Point(210, 298);
            this.tile5d.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile5d.Name = "tile5d";
            this.tile5d.Size = new System.Drawing.Size(60, 64);
            this.tile5d.TabIndex = 39;
            this.tile5d.Click += new System.EventHandler(this.tile5d_Click);
            // 
            // tile2h
            // 
            this.tile2h.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.tile2h.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile2h.Location = new System.Drawing.Point(474, 84);
            this.tile2h.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile2h.Name = "tile2h";
            this.tile2h.Size = new System.Drawing.Size(60, 64);
            this.tile2h.TabIndex = 24;
            this.tile2h.Click += new System.EventHandler(this.tile2h_Click);
            // 
            // tile6d
            // 
            this.tile6d.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.tile6d.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile6d.Location = new System.Drawing.Point(210, 368);
            this.tile6d.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile6d.Name = "tile6d";
            this.tile6d.Size = new System.Drawing.Size(60, 64);
            this.tile6d.TabIndex = 40;
            this.tile6d.Click += new System.EventHandler(this.tile6d_Click);
            // 
            // tile1h
            // 
            this.tile1h.BackColor = System.Drawing.Color.Moccasin;
            this.tile1h.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile1h.Location = new System.Drawing.Point(474, 13);
            this.tile1h.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile1h.Name = "tile1h";
            this.tile1h.Size = new System.Drawing.Size(60, 64);
            this.tile1h.TabIndex = 23;
            this.tile1h.Click += new System.EventHandler(this.tile1h_Click);
            // 
            // tile7c
            // 
            this.tile7c.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.tile7c.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile7c.Location = new System.Drawing.Point(144, 439);
            this.tile7c.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile7c.Name = "tile7c";
            this.tile7c.Size = new System.Drawing.Size(60, 64);
            this.tile7c.TabIndex = 41;
            this.tile7c.Click += new System.EventHandler(this.tile7c_Click);
            // 
            // tile3f
            // 
            this.tile3f.BackColor = System.Drawing.Color.Moccasin;
            this.tile3f.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile3f.Location = new System.Drawing.Point(342, 155);
            this.tile3f.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile3f.Name = "tile3f";
            this.tile3f.Size = new System.Drawing.Size(60, 64);
            this.tile3f.TabIndex = 22;
            this.tile3f.Click += new System.EventHandler(this.tile3f_Click);
            // 
            // tile7a
            // 
            this.tile7a.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.tile7a.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile7a.Location = new System.Drawing.Point(12, 439);
            this.tile7a.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile7a.Name = "tile7a";
            this.tile7a.Size = new System.Drawing.Size(60, 64);
            this.tile7a.TabIndex = 42;
            this.tile7a.Click += new System.EventHandler(this.tile7a_Click);
            // 
            // tile1g
            // 
            this.tile1g.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.tile1g.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile1g.Location = new System.Drawing.Point(408, 13);
            this.tile1g.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile1g.Name = "tile1g";
            this.tile1g.Size = new System.Drawing.Size(60, 64);
            this.tile1g.TabIndex = 21;
            this.tile1g.Click += new System.EventHandler(this.tile1g_Click);
            // 
            // tile8b
            // 
            this.tile8b.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.tile8b.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile8b.Location = new System.Drawing.Point(78, 510);
            this.tile8b.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile8b.Name = "tile8b";
            this.tile8b.Size = new System.Drawing.Size(60, 64);
            this.tile8b.TabIndex = 43;
            this.tile8b.Click += new System.EventHandler(this.tile8b_Click);
            // 
            // tile2f
            // 
            this.tile2f.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.tile2f.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile2f.Location = new System.Drawing.Point(342, 84);
            this.tile2f.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile2f.Name = "tile2f";
            this.tile2f.Size = new System.Drawing.Size(60, 64);
            this.tile2f.TabIndex = 20;
            this.tile2f.Click += new System.EventHandler(this.tile2f_Click);
            // 
            // tile7d
            // 
            this.tile7d.BackColor = System.Drawing.Color.Moccasin;
            this.tile7d.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile7d.Location = new System.Drawing.Point(210, 439);
            this.tile7d.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile7d.Name = "tile7d";
            this.tile7d.Size = new System.Drawing.Size(60, 64);
            this.tile7d.TabIndex = 44;
            this.tile7d.Click += new System.EventHandler(this.tile7d_Click);
            // 
            // tile2g
            // 
            this.tile2g.BackColor = System.Drawing.Color.Moccasin;
            this.tile2g.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile2g.Location = new System.Drawing.Point(408, 84);
            this.tile2g.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile2g.Name = "tile2g";
            this.tile2g.Size = new System.Drawing.Size(60, 64);
            this.tile2g.TabIndex = 19;
            this.tile2g.Click += new System.EventHandler(this.tile2g_Click);
            // 
            // tile8c
            // 
            this.tile8c.BackColor = System.Drawing.Color.Moccasin;
            this.tile8c.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile8c.Location = new System.Drawing.Point(144, 510);
            this.tile8c.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile8c.Name = "tile8c";
            this.tile8c.Size = new System.Drawing.Size(60, 64);
            this.tile8c.TabIndex = 45;
            this.tile8c.Click += new System.EventHandler(this.tile8c_Click);
            // 
            // tile2e
            // 
            this.tile2e.BackColor = System.Drawing.Color.Moccasin;
            this.tile2e.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile2e.Location = new System.Drawing.Point(276, 84);
            this.tile2e.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile2e.Name = "tile2e";
            this.tile2e.Size = new System.Drawing.Size(60, 64);
            this.tile2e.TabIndex = 18;
            this.tile2e.Click += new System.EventHandler(this.tile2e_Click);
            // 
            // tile8a
            // 
            this.tile8a.BackColor = System.Drawing.Color.Moccasin;
            this.tile8a.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile8a.Location = new System.Drawing.Point(12, 510);
            this.tile8a.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile8a.Name = "tile8a";
            this.tile8a.Size = new System.Drawing.Size(60, 64);
            this.tile8a.TabIndex = 46;
            this.tile8a.Click += new System.EventHandler(this.tile8a_Click);
            // 
            // tile1f
            // 
            this.tile1f.BackColor = System.Drawing.Color.Moccasin;
            this.tile1f.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile1f.Location = new System.Drawing.Point(342, 13);
            this.tile1f.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile1f.Name = "tile1f";
            this.tile1f.Size = new System.Drawing.Size(60, 64);
            this.tile1f.TabIndex = 17;
            this.tile1f.Click += new System.EventHandler(this.tile1f_Click);
            // 
            // tile8d
            // 
            this.tile8d.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.tile8d.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile8d.Location = new System.Drawing.Point(210, 510);
            this.tile8d.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile8d.Name = "tile8d";
            this.tile8d.Size = new System.Drawing.Size(60, 64);
            this.tile8d.TabIndex = 47;
            this.tile8d.Click += new System.EventHandler(this.tile8d_Click);
            // 
            // tile1e
            // 
            this.tile1e.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.tile1e.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile1e.Location = new System.Drawing.Point(276, 13);
            this.tile1e.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile1e.Name = "tile1e";
            this.tile1e.Size = new System.Drawing.Size(60, 64);
            this.tile1e.TabIndex = 16;
            this.tile1e.Click += new System.EventHandler(this.tile1e_Click);
            // 
            // tile5e
            // 
            this.tile5e.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.tile5e.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile5e.Location = new System.Drawing.Point(276, 298);
            this.tile5e.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile5e.Name = "tile5e";
            this.tile5e.Size = new System.Drawing.Size(60, 64);
            this.tile5e.TabIndex = 48;
            this.tile5e.Click += new System.EventHandler(this.tile5e_Click);
            // 
            // tile4d
            // 
            this.tile4d.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.tile4d.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile4d.Location = new System.Drawing.Point(210, 226);
            this.tile4d.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile4d.Name = "tile4d";
            this.tile4d.Size = new System.Drawing.Size(60, 64);
            this.tile4d.TabIndex = 15;
            this.tile4d.Click += new System.EventHandler(this.tile4d_Click);
            // 
            // tile5f
            // 
            this.tile5f.BackColor = System.Drawing.Color.Moccasin;
            this.tile5f.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile5f.Location = new System.Drawing.Point(342, 298);
            this.tile5f.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile5f.Name = "tile5f";
            this.tile5f.Size = new System.Drawing.Size(60, 64);
            this.tile5f.TabIndex = 49;
            this.tile5f.Click += new System.EventHandler(this.tile5f_Click);
            // 
            // tile4a
            // 
            this.tile4a.BackColor = System.Drawing.Color.Moccasin;
            this.tile4a.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile4a.Location = new System.Drawing.Point(12, 226);
            this.tile4a.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile4a.Name = "tile4a";
            this.tile4a.Size = new System.Drawing.Size(60, 64);
            this.tile4a.TabIndex = 14;
            this.tile4a.Click += new System.EventHandler(this.tile4a_Click);
            // 
            // tile6e
            // 
            this.tile6e.BackColor = System.Drawing.Color.Moccasin;
            this.tile6e.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile6e.Location = new System.Drawing.Point(276, 368);
            this.tile6e.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile6e.Name = "tile6e";
            this.tile6e.Size = new System.Drawing.Size(60, 64);
            this.tile6e.TabIndex = 50;
            this.tile6e.Click += new System.EventHandler(this.tile6e_Click);
            // 
            // tile4c
            // 
            this.tile4c.BackColor = System.Drawing.Color.Moccasin;
            this.tile4c.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile4c.Location = new System.Drawing.Point(144, 226);
            this.tile4c.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile4c.Name = "tile4c";
            this.tile4c.Size = new System.Drawing.Size(60, 64);
            this.tile4c.TabIndex = 13;
            this.tile4c.Click += new System.EventHandler(this.tile4c_Click);
            // 
            // tile6g
            // 
            this.tile6g.BackColor = System.Drawing.Color.Moccasin;
            this.tile6g.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile6g.Location = new System.Drawing.Point(408, 368);
            this.tile6g.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile6g.Name = "tile6g";
            this.tile6g.Size = new System.Drawing.Size(60, 64);
            this.tile6g.TabIndex = 51;
            this.tile6g.Click += new System.EventHandler(this.tile6g_Click);
            // 
            // tile3d
            // 
            this.tile3d.BackColor = System.Drawing.Color.Moccasin;
            this.tile3d.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile3d.Location = new System.Drawing.Point(210, 155);
            this.tile3d.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile3d.Name = "tile3d";
            this.tile3d.Size = new System.Drawing.Size(60, 64);
            this.tile3d.TabIndex = 12;
            this.tile3d.Click += new System.EventHandler(this.tile3d_Click);
            // 
            // tile6f
            // 
            this.tile6f.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.tile6f.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile6f.Location = new System.Drawing.Point(342, 368);
            this.tile6f.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile6f.Name = "tile6f";
            this.tile6f.Size = new System.Drawing.Size(60, 64);
            this.tile6f.TabIndex = 52;
            this.tile6f.Click += new System.EventHandler(this.tile6f_Click);
            // 
            // tile4b
            // 
            this.tile4b.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.tile4b.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile4b.Location = new System.Drawing.Point(78, 226);
            this.tile4b.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile4b.Name = "tile4b";
            this.tile4b.Size = new System.Drawing.Size(60, 64);
            this.tile4b.TabIndex = 11;
            this.tile4b.Click += new System.EventHandler(this.tile4b_Click);
            // 
            // tile5g
            // 
            this.tile5g.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.tile5g.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile5g.Location = new System.Drawing.Point(408, 298);
            this.tile5g.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile5g.Name = "tile5g";
            this.tile5g.Size = new System.Drawing.Size(60, 64);
            this.tile5g.TabIndex = 53;
            this.tile5g.Click += new System.EventHandler(this.tile5g_Click);
            // 
            // tile3a
            // 
            this.tile3a.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.tile3a.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile3a.Location = new System.Drawing.Point(12, 155);
            this.tile3a.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile3a.Name = "tile3a";
            this.tile3a.Size = new System.Drawing.Size(60, 64);
            this.tile3a.TabIndex = 10;
            this.tile3a.Click += new System.EventHandler(this.tile3a_Click);
            // 
            // tile7f
            // 
            this.tile7f.BackColor = System.Drawing.Color.Moccasin;
            this.tile7f.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile7f.Location = new System.Drawing.Point(342, 439);
            this.tile7f.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile7f.Name = "tile7f";
            this.tile7f.Size = new System.Drawing.Size(60, 64);
            this.tile7f.TabIndex = 54;
            this.tile7f.Click += new System.EventHandler(this.tile7f_Click);
            // 
            // tile3c
            // 
            this.tile3c.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.tile3c.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile3c.Location = new System.Drawing.Point(144, 155);
            this.tile3c.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile3c.Name = "tile3c";
            this.tile3c.Size = new System.Drawing.Size(60, 64);
            this.tile3c.TabIndex = 9;
            this.tile3c.Click += new System.EventHandler(this.tile3c_Click);
            // 
            // tile5h
            // 
            this.tile5h.BackColor = System.Drawing.Color.Moccasin;
            this.tile5h.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile5h.Location = new System.Drawing.Point(474, 298);
            this.tile5h.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile5h.Name = "tile5h";
            this.tile5h.Size = new System.Drawing.Size(60, 64);
            this.tile5h.TabIndex = 55;
            this.tile5h.Click += new System.EventHandler(this.tile5h_Click);
            // 
            // tile2d
            // 
            this.tile2d.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.tile2d.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile2d.Location = new System.Drawing.Point(210, 84);
            this.tile2d.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile2d.Name = "tile2d";
            this.tile2d.Size = new System.Drawing.Size(60, 64);
            this.tile2d.TabIndex = 8;
            this.tile2d.Click += new System.EventHandler(this.tile2d_Click);
            // 
            // tile6h
            // 
            this.tile6h.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.tile6h.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile6h.Location = new System.Drawing.Point(474, 368);
            this.tile6h.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile6h.Name = "tile6h";
            this.tile6h.Size = new System.Drawing.Size(60, 64);
            this.tile6h.TabIndex = 56;
            this.tile6h.Click += new System.EventHandler(this.tile6h_Click);
            // 
            // tile1d
            // 
            this.tile1d.BackColor = System.Drawing.Color.Moccasin;
            this.tile1d.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile1d.Location = new System.Drawing.Point(210, 13);
            this.tile1d.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile1d.Name = "tile1d";
            this.tile1d.Size = new System.Drawing.Size(60, 64);
            this.tile1d.TabIndex = 7;
            this.tile1d.Click += new System.EventHandler(this.tile1d_Click);
            // 
            // tile7g
            // 
            this.tile7g.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.tile7g.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile7g.Location = new System.Drawing.Point(408, 439);
            this.tile7g.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile7g.Name = "tile7g";
            this.tile7g.Size = new System.Drawing.Size(60, 64);
            this.tile7g.TabIndex = 57;
            this.tile7g.Click += new System.EventHandler(this.tile7g_Click);
            // 
            // tile3b
            // 
            this.tile3b.BackColor = System.Drawing.Color.Moccasin;
            this.tile3b.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile3b.Location = new System.Drawing.Point(78, 155);
            this.tile3b.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile3b.Name = "tile3b";
            this.tile3b.Size = new System.Drawing.Size(60, 64);
            this.tile3b.TabIndex = 6;
            this.tile3b.Click += new System.EventHandler(this.tile3b_Click);
            // 
            // tile7e
            // 
            this.tile7e.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.tile7e.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile7e.Location = new System.Drawing.Point(276, 439);
            this.tile7e.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile7e.Name = "tile7e";
            this.tile7e.Size = new System.Drawing.Size(60, 64);
            this.tile7e.TabIndex = 58;
            this.tile7e.Click += new System.EventHandler(this.tile7e_Click);
            // 
            // tile1c
            // 
            this.tile1c.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.tile1c.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile1c.Location = new System.Drawing.Point(144, 13);
            this.tile1c.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile1c.Name = "tile1c";
            this.tile1c.Size = new System.Drawing.Size(60, 64);
            this.tile1c.TabIndex = 5;
            this.tile1c.Click += new System.EventHandler(this.tile1c_Click);
            // 
            // tile8f
            // 
            this.tile8f.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.tile8f.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile8f.Location = new System.Drawing.Point(342, 510);
            this.tile8f.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile8f.Name = "tile8f";
            this.tile8f.Size = new System.Drawing.Size(60, 64);
            this.tile8f.TabIndex = 59;
            this.tile8f.Click += new System.EventHandler(this.tile8f_Click);
            // 
            // tile2b
            // 
            this.tile2b.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.tile2b.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile2b.Location = new System.Drawing.Point(78, 84);
            this.tile2b.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile2b.Name = "tile2b";
            this.tile2b.Size = new System.Drawing.Size(60, 64);
            this.tile2b.TabIndex = 4;
            this.tile2b.Click += new System.EventHandler(this.tile2b_Click);
            // 
            // tile7h
            // 
            this.tile7h.BackColor = System.Drawing.Color.Moccasin;
            this.tile7h.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile7h.Location = new System.Drawing.Point(474, 439);
            this.tile7h.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile7h.Name = "tile7h";
            this.tile7h.Size = new System.Drawing.Size(60, 64);
            this.tile7h.TabIndex = 60;
            this.tile7h.Click += new System.EventHandler(this.tile7h_Click);
            // 
            // tile2c
            // 
            this.tile2c.BackColor = System.Drawing.Color.Moccasin;
            this.tile2c.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile2c.Location = new System.Drawing.Point(144, 84);
            this.tile2c.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile2c.Name = "tile2c";
            this.tile2c.Size = new System.Drawing.Size(60, 64);
            this.tile2c.TabIndex = 3;
            this.tile2c.Click += new System.EventHandler(this.tile2c_Click);
            // 
            // tile8g
            // 
            this.tile8g.BackColor = System.Drawing.Color.Moccasin;
            this.tile8g.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile8g.Location = new System.Drawing.Point(408, 510);
            this.tile8g.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile8g.Name = "tile8g";
            this.tile8g.Size = new System.Drawing.Size(60, 64);
            this.tile8g.TabIndex = 61;
            this.tile8g.Click += new System.EventHandler(this.tile8g_Click);
            // 
            // tile2a
            // 
            this.tile2a.BackColor = System.Drawing.Color.Moccasin;
            this.tile2a.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile2a.Location = new System.Drawing.Point(12, 84);
            this.tile2a.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile2a.Name = "tile2a";
            this.tile2a.Size = new System.Drawing.Size(60, 64);
            this.tile2a.TabIndex = 2;
            this.tile2a.Click += new System.EventHandler(this.tile2a_Click);
            // 
            // tile8e
            // 
            this.tile8e.BackColor = System.Drawing.Color.Moccasin;
            this.tile8e.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile8e.Location = new System.Drawing.Point(276, 510);
            this.tile8e.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile8e.Name = "tile8e";
            this.tile8e.Size = new System.Drawing.Size(60, 64);
            this.tile8e.TabIndex = 62;
            this.tile8e.Click += new System.EventHandler(this.tile8e_Click);
            // 
            // tile1b
            // 
            this.tile1b.BackColor = System.Drawing.Color.Moccasin;
            this.tile1b.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile1b.Location = new System.Drawing.Point(78, 13);
            this.tile1b.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile1b.Name = "tile1b";
            this.tile1b.Size = new System.Drawing.Size(60, 64);
            this.tile1b.TabIndex = 1;
            this.tile1b.Click += new System.EventHandler(this.tile1b_Click);
            // 
            // tile8h
            // 
            this.tile8h.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.tile8h.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile8h.Location = new System.Drawing.Point(474, 510);
            this.tile8h.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile8h.Name = "tile8h";
            this.tile8h.Size = new System.Drawing.Size(60, 64);
            this.tile8h.TabIndex = 63;
            this.tile8h.Click += new System.EventHandler(this.tile8h_Click);
            // 
            // tile1a
            // 
            this.tile1a.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.tile1a.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tile1a.Location = new System.Drawing.Point(12, 13);
            this.tile1a.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tile1a.Name = "tile1a";
            this.tile1a.Size = new System.Drawing.Size(60, 64);
            this.tile1a.TabIndex = 0;
            this.tile1a.Click += new System.EventHandler(this.tile1a_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.Font = new System.Drawing.Font("Times New Roman", 14F);
            this.CancelButton.Location = new System.Drawing.Point(566, 419);
            this.CancelButton.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(160, 84);
            this.CancelButton.TabIndex = 65;
            this.CancelButton.TabStop = false;
            this.CancelButton.Text = "Cancel";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // ConcedeButton
            // 
            this.ConcedeButton.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConcedeButton.Location = new System.Drawing.Point(732, 419);
            this.ConcedeButton.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.ConcedeButton.Name = "ConcedeButton";
            this.ConcedeButton.Size = new System.Drawing.Size(160, 84);
            this.ConcedeButton.TabIndex = 65;
            this.ConcedeButton.TabStop = false;
            this.ConcedeButton.Text = "Forfeit";
            this.ConcedeButton.UseVisualStyleBackColor = true;
            this.ConcedeButton.Click += new System.EventHandler(this.ConcedeButton_Click);
            // 
            // OutputBox
            // 
            this.OutputBox.Font = new System.Drawing.Font("Times New Roman", 14F);
            this.OutputBox.Location = new System.Drawing.Point(566, 298);
            this.OutputBox.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.OutputBox.Multiline = true;
            this.OutputBox.Name = "OutputBox";
            this.OutputBox.ReadOnly = true;
            this.OutputBox.Size = new System.Drawing.Size(326, 111);
            this.OutputBox.TabIndex = 66;
            this.OutputBox.TabStop = false;
            this.OutputBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 14F);
            this.label1.Location = new System.Drawing.Point(562, 127);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 21);
            this.label1.TabIndex = 67;
            this.label1.Text = "Odds:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 14F);
            this.label2.Location = new System.Drawing.Point(564, 170);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 21);
            this.label2.TabIndex = 68;
            this.label2.Text = "White";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 14F);
            this.label3.Location = new System.Drawing.Point(566, 198);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 21);
            this.label3.TabIndex = 69;
            this.label3.Text = "Black";
            // 
            // WhiteOdds
            // 
            this.WhiteOdds.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.WhiteOdds.Location = new System.Drawing.Point(624, 168);
            this.WhiteOdds.Name = "WhiteOdds";
            this.WhiteOdds.ReadOnly = true;
            this.WhiteOdds.Size = new System.Drawing.Size(47, 26);
            this.WhiteOdds.TabIndex = 70;
            this.WhiteOdds.TabStop = false;
            this.WhiteOdds.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // BlackOdds
            // 
            this.BlackOdds.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.BlackOdds.Location = new System.Drawing.Point(624, 196);
            this.BlackOdds.Name = "BlackOdds";
            this.BlackOdds.ReadOnly = true;
            this.BlackOdds.Size = new System.Drawing.Size(47, 26);
            this.BlackOdds.TabIndex = 71;
            this.BlackOdds.TabStop = false;
            this.BlackOdds.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 14F);
            this.label4.Location = new System.Drawing.Point(677, 171);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(26, 21);
            this.label4.TabIndex = 72;
            this.label4.Text = "%";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 14F);
            this.label5.Location = new System.Drawing.Point(677, 199);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(26, 21);
            this.label5.TabIndex = 73;
            this.label5.Text = "%";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.label6.Location = new System.Drawing.Point(649, 563);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(265, 16);
            this.label6.TabIndex = 74;
            this.label6.Text = "AntiChess game by Jin Hoe Lim - 101458985";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Font = new System.Drawing.Font("Times New Roman", 14F);
            this.checkBox1.Location = new System.Drawing.Point(566, 265);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(167, 25);
            this.checkBox1.TabIndex = 75;
            this.checkBox1.Text = "Colourblind Mode";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Times New Roman", 14F);
            this.label7.Location = new System.Drawing.Point(728, 127);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(97, 21);
            this.label7.TabIndex = 76;
            this.label7.Text = "Pieces Left:";
            // 
            // BlackScore
            // 
            this.BlackScore.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.BlackScore.Location = new System.Drawing.Point(788, 196);
            this.BlackScore.Name = "BlackScore";
            this.BlackScore.ReadOnly = true;
            this.BlackScore.Size = new System.Drawing.Size(47, 26);
            this.BlackScore.TabIndex = 80;
            this.BlackScore.TabStop = false;
            this.BlackScore.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // WhiteScore
            // 
            this.WhiteScore.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.WhiteScore.Location = new System.Drawing.Point(788, 168);
            this.WhiteScore.Name = "WhiteScore";
            this.WhiteScore.ReadOnly = true;
            this.WhiteScore.Size = new System.Drawing.Size(47, 26);
            this.WhiteScore.TabIndex = 79;
            this.WhiteScore.TabStop = false;
            this.WhiteScore.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Times New Roman", 14F);
            this.label10.Location = new System.Drawing.Point(730, 198);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(52, 21);
            this.label10.TabIndex = 78;
            this.label10.Text = "Black";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Times New Roman", 14F);
            this.label11.Location = new System.Drawing.Point(728, 170);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(54, 21);
            this.label11.TabIndex = 77;
            this.label11.Text = "White";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Times New Roman", 14F);
            this.label8.Location = new System.Drawing.Point(841, 170);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 21);
            this.label8.TabIndex = 81;
            this.label8.Text = "/ 16";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Times New Roman", 14F);
            this.label9.Location = new System.Drawing.Point(841, 199);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(38, 21);
            this.label9.TabIndex = 82;
            this.label9.Text = "/ 16";
            // 
            // GameScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(926, 588);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.BlackScore);
            this.Controls.Add(this.WhiteScore);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.BlackOdds);
            this.Controls.Add(this.WhiteOdds);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.OutputBox);
            this.Controls.Add(this.ConcedeButton);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.tile1a);
            this.Controls.Add(this.tile8h);
            this.Controls.Add(this.tile1b);
            this.Controls.Add(this.tile5a);
            this.Controls.Add(this.tile8e);
            this.Controls.Add(this.tile4h);
            this.Controls.Add(this.tile2a);
            this.Controls.Add(this.tile5b);
            this.Controls.Add(this.tile8g);
            this.Controls.Add(this.tile4e);
            this.Controls.Add(this.tile2c);
            this.Controls.Add(this.tile6a);
            this.Controls.Add(this.tile7h);
            this.Controls.Add(this.tile4g);
            this.Controls.Add(this.tile2b);
            this.Controls.Add(this.tile6c);
            this.Controls.Add(this.tile8f);
            this.Controls.Add(this.tile3h);
            this.Controls.Add(this.tile1c);
            this.Controls.Add(this.tile6b);
            this.Controls.Add(this.tile7e);
            this.Controls.Add(this.tile4f);
            this.Controls.Add(this.tile3b);
            this.Controls.Add(this.tile5c);
            this.Controls.Add(this.tile7g);
            this.Controls.Add(this.tile3e);
            this.Controls.Add(this.tile1d);
            this.Controls.Add(this.tile7b);
            this.Controls.Add(this.tile6h);
            this.Controls.Add(this.tile3g);
            this.Controls.Add(this.tile2d);
            this.Controls.Add(this.tile5d);
            this.Controls.Add(this.tile5h);
            this.Controls.Add(this.tile2h);
            this.Controls.Add(this.tile3c);
            this.Controls.Add(this.tile6d);
            this.Controls.Add(this.tile7f);
            this.Controls.Add(this.tile1h);
            this.Controls.Add(this.tile3a);
            this.Controls.Add(this.tile7c);
            this.Controls.Add(this.tile5g);
            this.Controls.Add(this.tile3f);
            this.Controls.Add(this.tile4b);
            this.Controls.Add(this.tile7a);
            this.Controls.Add(this.tile6f);
            this.Controls.Add(this.tile1g);
            this.Controls.Add(this.tile3d);
            this.Controls.Add(this.tile8b);
            this.Controls.Add(this.tile6g);
            this.Controls.Add(this.tile2f);
            this.Controls.Add(this.tile4c);
            this.Controls.Add(this.tile7d);
            this.Controls.Add(this.tile6e);
            this.Controls.Add(this.tile2g);
            this.Controls.Add(this.tile4a);
            this.Controls.Add(this.tile8c);
            this.Controls.Add(this.tile5f);
            this.Controls.Add(this.tile2e);
            this.Controls.Add(this.tile4d);
            this.Controls.Add(this.tile8a);
            this.Controls.Add(this.tile5e);
            this.Controls.Add(this.tile1f);
            this.Controls.Add(this.tile1e);
            this.Controls.Add(this.tile8d);
            this.Font = new System.Drawing.Font("Times New Roman", 8.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Name = "GameScreen";
            this.Text = "AntiChess";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel tile5a;
        private System.Windows.Forms.Panel tile4h;
        private System.Windows.Forms.Panel tile5b;
        private System.Windows.Forms.Panel tile4e;
        private System.Windows.Forms.Panel tile6a;
        private System.Windows.Forms.Panel tile4g;
        private System.Windows.Forms.Panel tile6c;
        private System.Windows.Forms.Panel tile3h;
        private System.Windows.Forms.Panel tile6b;
        private System.Windows.Forms.Panel tile4f;
        private System.Windows.Forms.Panel tile5c;
        private System.Windows.Forms.Panel tile3e;
        private System.Windows.Forms.Panel tile7b;
        private System.Windows.Forms.Panel tile3g;
        private System.Windows.Forms.Panel tile5d;
        private System.Windows.Forms.Panel tile2h;
        private System.Windows.Forms.Panel tile6d;
        private System.Windows.Forms.Panel tile1h;
        private System.Windows.Forms.Panel tile7c;
        private System.Windows.Forms.Panel tile3f;
        private System.Windows.Forms.Panel tile7a;
        private System.Windows.Forms.Panel tile1g;
        private System.Windows.Forms.Panel tile8b;
        private System.Windows.Forms.Panel tile2f;
        private System.Windows.Forms.Panel tile7d;
        private System.Windows.Forms.Panel tile2g;
        private System.Windows.Forms.Panel tile8c;
        private System.Windows.Forms.Panel tile2e;
        private System.Windows.Forms.Panel tile8a;
        private System.Windows.Forms.Panel tile1f;
        private System.Windows.Forms.Panel tile8d;
        private System.Windows.Forms.Panel tile1e;
        private System.Windows.Forms.Panel tile5e;
        private System.Windows.Forms.Panel tile4d;
        private System.Windows.Forms.Panel tile5f;
        private System.Windows.Forms.Panel tile4a;
        private System.Windows.Forms.Panel tile6e;
        private System.Windows.Forms.Panel tile4c;
        private System.Windows.Forms.Panel tile6g;
        private System.Windows.Forms.Panel tile3d;
        private System.Windows.Forms.Panel tile6f;
        private System.Windows.Forms.Panel tile4b;
        private System.Windows.Forms.Panel tile5g;
        private System.Windows.Forms.Panel tile3a;
        private System.Windows.Forms.Panel tile7f;
        private System.Windows.Forms.Panel tile3c;
        private System.Windows.Forms.Panel tile5h;
        private System.Windows.Forms.Panel tile2d;
        private System.Windows.Forms.Panel tile6h;
        private System.Windows.Forms.Panel tile1d;
        private System.Windows.Forms.Panel tile7g;
        private System.Windows.Forms.Panel tile3b;
        private System.Windows.Forms.Panel tile7e;
        private System.Windows.Forms.Panel tile1c;
        private System.Windows.Forms.Panel tile8f;
        private System.Windows.Forms.Panel tile2b;
        private System.Windows.Forms.Panel tile7h;
        private System.Windows.Forms.Panel tile2c;
        private System.Windows.Forms.Panel tile8g;
        private System.Windows.Forms.Panel tile2a;
        private System.Windows.Forms.Panel tile8e;
        private System.Windows.Forms.Panel tile1b;
        private System.Windows.Forms.Panel tile8h;
        private System.Windows.Forms.Panel tile1a;
        private System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.Button ConcedeButton;
        private System.Windows.Forms.TextBox OutputBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox WhiteOdds;
        private System.Windows.Forms.TextBox BlackOdds;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox BlackScore;
        private System.Windows.Forms.TextBox WhiteScore;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
    }
}

