﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AntiChess
{
    class Queen : Piece
    {
        public Queen(bool colour, Image icon) : base(colour, "Queen", icon)
        {
        }

        public override bool CanCapture(Tile[,] _chess, int x, int y) //Combines the methods of the Bishop and Rook classes, as the Queen behaves as both.
        {
            int _xCount, _yCount;
            bool _canCapture = false;

            _xCount = x;
            _yCount = y;
            while (_xCount >= 0 && _yCount >= 0 && !_canCapture) //Checks upper left path of Queen
            {
                if (_chess[_xCount, _yCount].GetPiece != null && _chess[_xCount, _yCount].GetPiece.Colour != Colour) //If the tile has a piece that is the opposite colour
                    _canCapture = true; //This flag will end the loop and prevent any following loops from starting
                else if (_chess[_xCount, _yCount].GetPiece != null && _chess[_xCount, _yCount].GetPiece.Colour == Colour && _chess[_xCount, _yCount].GetPiece != this)
                    break;

                _xCount--;
                _yCount--;
            }

            _xCount = x; //X and Y references are reset every path to the location of the piece
            _yCount = y;
            while (_xCount >= 0 && _yCount <= 7 && !_canCapture) //Checks lower left path of Queen
            {
                if (_chess[_xCount, _yCount].GetPiece != null && _chess[_xCount, _yCount].GetPiece.Colour != Colour)
                    _canCapture = true;
                else if (_chess[_xCount, _yCount].GetPiece != null && _chess[_xCount, _yCount].GetPiece.Colour == Colour && _chess[_xCount, _yCount].GetPiece != this)
                    break;

                _xCount--;
                _yCount++;
            }

            _xCount = x;
            _yCount = y;
            while (_xCount <= 7 && _yCount >= 0 && !_canCapture) //Checks upper right path of Queen
            {
                if (_chess[_xCount, _yCount].GetPiece != null && _chess[_xCount, _yCount].GetPiece.Colour != Colour)
                    _canCapture = true;
                else if (_chess[_xCount, _yCount].GetPiece != null && _chess[_xCount, _yCount].GetPiece.Colour == Colour && _chess[_xCount, _yCount].GetPiece != this)
                    break;

                _xCount++;
                _yCount--;
            }

            _xCount = x;
            _yCount = y;
            while (_xCount <= 7 && _yCount <= 7 && !_canCapture) //Checks lower right path of Queen
            {
                if (_chess[_xCount, _yCount].GetPiece != null && _chess[_xCount, _yCount].GetPiece.Colour != Colour)
                    _canCapture = true;
                else if (_chess[_xCount, _yCount].GetPiece != null && _chess[_xCount, _yCount].GetPiece.Colour == Colour && _chess[_xCount, _yCount].GetPiece != this)
                    break;

                _xCount++;
                _yCount++;
            }

            for (int i = x; i >= 0; i--) //Checks left of Queen
            {
                if (_chess[i, y].GetPiece != null && _chess[i, y].GetPiece.Colour != Colour) //If it is the opposite colour of the current piece
                {
                    _canCapture = true;
                    break;
                }
                else if (_chess[i, y].GetPiece != null && _chess[i, y].GetPiece.Colour == Colour && _chess[i, y].GetPiece != this)
                    break;
            }

            for (int i = x; i <= 7; i++) //Checks right of Queen
            {
                if (_chess[i, y].GetPiece != null && _chess[i, y].GetPiece.Colour != Colour)
                {
                    _canCapture = true;
                    break;
                }
                else if (_chess[i, y].GetPiece != null && _chess[i, y].GetPiece.Colour == Colour && _chess[i, y].GetPiece != this)
                    break;
            }

            for (int i = y; i >= 0; i--) //Checks upwards of Queen
            {
                if (_chess[x, i].GetPiece != null && _chess[x, i].GetPiece.Colour != Colour)
                {
                    _canCapture = true;
                    break;
                }
                else if (_chess[x, i].GetPiece != null && _chess[x, i].GetPiece.Colour == Colour && _chess[x, i].GetPiece != this)
                    break;
            }

            for (int i = y; i <= 7; i++) //Checks downwards of Queen
            {
                if (_chess[x, i].GetPiece != null && _chess[x, i].GetPiece.Colour != Colour)
                {
                    _canCapture = true;
                    break;
                }
                else if (_chess[x, i].GetPiece != null && _chess[x, i].GetPiece.Colour == Colour && _chess[x, i].GetPiece != this)
                    break;
            }

            return _canCapture;
        }

        public override void HighlightMove(Board _chess, int x, int y) //Combines the methods of the Bishop and Rook classes, as the Queen behaves as both.
        {
            int _xCount, _yCount;

            _xCount = x;
            _yCount = y;
            while (_xCount >= 0 && _yCount >= 0) //Highlights upper left path of Queen
            {
                if (_chess.Map[_xCount, _yCount].GetPiece == null) //If tile is empty, then it can be moved to
                {
                    _chess.Map[_xCount, _yCount].Selectable = true;
                }
                else if (_chess.Map[_xCount, _yCount].GetPiece != this) //Otherwise it's not empty, so end the loop
                    break;

                _xCount--;
                _yCount--;
            }

            _xCount = x;
            _yCount = y;
            while (_xCount >= 0 && _yCount <= 7) //Highlights lower left path of Queen
            {
                if (_chess.Map[_xCount, _yCount].GetPiece == null)
                {
                    _chess.Map[_xCount, _yCount].Selectable = true;
                }
                else if (_chess.Map[_xCount, _yCount].GetPiece != this)
                    break;

                _xCount--;
                _yCount++;
            }

            _xCount = x;
            _yCount = y;
            while (_xCount <= 7 && _yCount >= 0) //Highlights upper right path of Queen
            {
                if (_chess.Map[_xCount, _yCount].GetPiece == null)
                {
                    _chess.Map[_xCount, _yCount].Selectable = true;
                }
                else if (_chess.Map[_xCount, _yCount].GetPiece != this)
                    break;

                _xCount++;
                _yCount--;
            }

            _xCount = x;
            _yCount = y;
            while (_xCount <= 7 && _yCount <= 7) //Highlights lower right path of Queen
            {
                if (_chess.Map[_xCount, _yCount].GetPiece == null)
                {
                    _chess.Map[_xCount, _yCount].Selectable = true;
                }
                else if (_chess.Map[_xCount, _yCount].GetPiece != this)
                    break;

                _xCount++;
                _yCount++;
            }

            for (int i = x; i >= 0; i--) //Highlghts left of Queen
            {
                if (_chess.Map[i, y].GetPiece == null) //If there are no pieces in that tile, then it can be moved to
                {
                    _chess.Map[i, y].Selectable = true;
                }
                else if (_chess.Map[i, y].GetPiece != this)
                    break;
            }

            for (int i = x; i <= 7; i++) //Highlghts right of Queen
            {
                if (_chess.Map[i, y].GetPiece == null)
                {
                    _chess.Map[i, y].Selectable = true;
                }
                else if (_chess.Map[i, y].GetPiece != this)
                    break;
            }

            for (int i = y; i >= 0; i--) //Highlghts upwards of Queen
            {
                if (_chess.Map[x, i].GetPiece == null)
                {
                    _chess.Map[x, i].Selectable = true;
                }
                else if (_chess.Map[x, i].GetPiece != this)
                    break;
            }

            for (int i = y; i <= 7; i++) //Highlghts downwards of Queen
            {
                if (_chess.Map[x, i].GetPiece == null)
                {
                    _chess.Map[x, i].Selectable = true;
                }
                else if (_chess.Map[x, i].GetPiece != this)
                    break;
            }
        }

        public override void HighlightCapture(Board _chess, int x, int y) //Combines the methods of the Bishop and Rook classes, as the Queen behaves as both.
        {
            int _xCount, _yCount;

            _xCount = x;
            _yCount = y;
            while (_xCount >= 0 && _yCount >= 0) //Highlights pieces on upper left path of Queen
            {
                if (_chess.Map[_xCount, _yCount].GetPiece != null && _chess.Map[_xCount, _yCount].GetPiece.Colour != Colour) //If the tile has a piece that is the opposite colour
                {
                    _chess.Map[_xCount, _yCount].Selectable = true; //Make it selectable
                    break; //Then break the loop to stop checking past it
                }
                else if (_chess.Map[_xCount, _yCount].GetPiece != null && _chess.Map[_xCount, _yCount].GetPiece.Colour == Colour && _chess.Map[_xCount, _yCount].GetPiece != this)
                    break;

                _xCount--;
                _yCount--;
            }

            _xCount = x;
            _yCount = y;
            while (_xCount >= 0 && _yCount <= 7) //Highlights pieces on lower left path of Queen
            {
                if (_chess.Map[_xCount, _yCount].GetPiece != null && _chess.Map[_xCount, _yCount].GetPiece.Colour != Colour)
                {
                    _chess.Map[_xCount, _yCount].Selectable = true;
                    break;
                }
                else if (_chess.Map[_xCount, _yCount].GetPiece != null && _chess.Map[_xCount, _yCount].GetPiece.Colour == Colour && _chess.Map[_xCount, _yCount].GetPiece != this)
                    break;

                _xCount--;
                _yCount++;
            }

            _xCount = x;
            _yCount = y;
            while (_xCount <= 7 && _yCount >= 0) //Highlights pieces on upper right path of Queen
            {
                if (_chess.Map[_xCount, _yCount].GetPiece != null && _chess.Map[_xCount, _yCount].GetPiece.Colour != Colour)
                {
                    _chess.Map[_xCount, _yCount].Selectable = true;
                    break;
                }
                else if (_chess.Map[_xCount, _yCount].GetPiece != null && _chess.Map[_xCount, _yCount].GetPiece.Colour == Colour && _chess.Map[_xCount, _yCount].GetPiece != this)
                    break;

                _xCount++;
                _yCount--;
            }

            _xCount = x;
            _yCount = y;
            while (_xCount <= 7 && _yCount <= 7) //Highlights pieces on lower right path of Queen
            {
                if (_chess.Map[_xCount, _yCount].GetPiece != null && _chess.Map[_xCount, _yCount].GetPiece.Colour != Colour)
                {
                    _chess.Map[_xCount, _yCount].Selectable = true;
                    break;
                }
                else if (_chess.Map[_xCount, _yCount].GetPiece != null && _chess.Map[_xCount, _yCount].GetPiece.Colour == Colour && _chess.Map[_xCount, _yCount].GetPiece != this)
                    break;

                _xCount++;
                _yCount++;
            }

            for (int i = x; i >= 0; i--) //Checks left of Queen
            {
                if (_chess.Map[i, y].GetPiece != null && _chess.Map[i, y].GetPiece.Colour != Colour) //If it is the opposite colour of the current piece
                {
                    _chess.Map[i, y].Selectable = true;
                    break;
                }
                else if (_chess.Map[i, y].GetPiece != null && _chess.Map[i, y].GetPiece.Colour == Colour && _chess.Map[i, y].GetPiece != this)
                    break;
            }

            for (int i = x; i <= 7; i++) //Checks right of Queen
            {
                if (_chess.Map[i, y].GetPiece != null && _chess.Map[i, y].GetPiece.Colour != Colour)
                {
                    _chess.Map[i, y].Selectable = true;
                    break;
                }
                else if (_chess.Map[i, y].GetPiece != null && _chess.Map[i, y].GetPiece.Colour == Colour && _chess.Map[i, y].GetPiece != this)
                    break;
            }

            for (int i = y; i >= 0; i--) //Checks upwards of Queen
            {
                if (_chess.Map[x, i].GetPiece != null && _chess.Map[x, i].GetPiece.Colour != Colour)
                {
                    _chess.Map[x, i].Selectable = true;
                    break;
                }
                else if (_chess.Map[x, i].GetPiece != null && _chess.Map[x, i].GetPiece.Colour == Colour && _chess.Map[x, i].GetPiece != this)
                    break;
            }

            for (int i = y; i <= 7; i++) //Checks downwards of Queen
            {
                if (_chess.Map[x, i].GetPiece != null && _chess.Map[x, i].GetPiece.Colour != Colour)
                {
                    _chess.Map[x, i].Selectable = true;
                    break;
                }
                else if (_chess.Map[x, i].GetPiece != null && _chess.Map[x, i].GetPiece.Colour == Colour && _chess.Map[x, i].GetPiece != this)
                    break;
            }
        }
    }
}
