﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;

namespace AntiChess
{
    class Game
    {
        Player _player1;
        Player _player2;
        Player _currentPlayer;
        Board _chess;
        GameMode _gm;
        bool _activeplayer;
        int _holdingX;
        int _holdingY;

        public Game()
        {
            _player1 = new Player();
            _player2 = new Player();
            _chess = new Board();

            _currentPlayer = _player1;

            _activeplayer = false; //White = false, Black = true
            _holdingX = -1;
            _holdingY = -1;
            _gm = GameMode.selecting;
        }

        void SwitchPlayer()
        {
            _activeplayer = !_activeplayer; //Switches the current player flag

            if (!_activeplayer) //Uses the flag to determine which player is the current player to be used
                _currentPlayer = _player1;
            else
                _currentPlayer = _player2;
        }

        public void ProcessTile(Tile _tile, int x, int y)
        {
            bool _actionTaken = false; //Has an action been taken? (Flag is used to switch players)

            if (_tile != null)
                switch (_gm)
                {
                    case GameMode.selecting: //The tile is put through the selection process based on the current game state
                        ProcessSelecting(_tile, _currentPlayer, x, y);
                        break;

                    case GameMode.moving: //The tile is put through the moving process based on the current game state
                        _actionTaken = ProcessMoving(_tile, _currentPlayer, x, y);
                        ResetAllTiles();
                        _gm = GameMode.selecting;
                        break;

                    case GameMode.capturing: //The tile is put through the capturing process based on the current game state
                        _actionTaken = ProcessCapturing(_tile, _currentPlayer, x, y);
                        ResetAllTiles();
                        _gm = GameMode.selecting;
                        break;
                }

            if (_actionTaken) //If an action has been made (Moved or captured), switch the player
            {
                SwitchPlayer();
            }

            _currentPlayer.HighlightCapturable(_chess.Map);

            UpgradePawns(_chess.Map);
        }

        void UpgradePawns(Tile[,] _chess) //Checks the top and bottom of the rows and upgrades pawns where appropriate
        {
            for (int x = 0; x <= 7; x++)
            {
                if (!_activeplayer)
                    if (_chess[x, 0].GetPiece != null && !_chess[x,0].GetPiece.Colour && _chess[x, 0].GetPiece is Pawn) //If a white pawn has reached the opposite side of the board
                        UpgradePawn(_player1, _chess[x, 0]);

                else if (_chess[x, 7].GetPiece != null && _chess[x, 7].GetPiece.Colour && _chess[x, 7].GetPiece is Pawn) //If a black pawn has reached the opposite side of the board
                        UpgradePawn(_player2, _chess[x, 7]);
            }
        }

        void UpgradePawn(Player _p, Tile _tile) //Changes the pawn to a king of the same player
        {
            Image _kingImage;

            if (_tile.GetPiece.Colour) //Determines the icon of the new king piece (colour)
                _kingImage = Properties.Resources.BlackKing;
            else
                _kingImage = Properties.Resources.WhiteKing;

            King _newKing = new King(_tile.GetPiece.Colour, _kingImage); //Creates the new king piece

            _p.Pieces.Remove(_tile.GetPiece);   //Removes the pawn
            _p.Pieces.Add(_newKing);            //Adds the new king piece to the player's piece list

            _tile.GetPiece = _newKing;          //Assigns the new king piece to the tile
        }

        public void ResetAllTiles() //Resets the IsActive and Selectable status for all files on the board
        {
            foreach (Tile _tile in _chess.Map)
            {
                _tile.IsActive = false;
                _tile.Selectable = false;
            }
        }

        void ProcessSelecting(Tile _tile, Player _p, int x, int y) //Processes the tile if it was "selected" (Determined by game mode)
        {
            _holdingX = x;
            _holdingY = y;
            if (_tile.GetPiece != null) //If there is a piece in the tile
            {
                if (_p.IsFree(_chess.Map)) //If the current player is free to move
                {
                    if (_tile.GetPiece.Colour == _activeplayer) //If the tile belongs to the current player
                    {
                        _tile.IsActive = true;
                        _tile.GetPiece.HighlightMove(_chess, x, y);
                        _gm = GameMode.moving;
                    }
                }
                else if (_tile.Selectable) //If the tile is selectable (Will only occur when tile can capture)
                {
                    _tile.IsActive = true;
                    _tile.GetPiece.HighlightCapture(_chess, x, y);
                    _gm = GameMode.capturing;
                }
            }
        }

        bool ProcessMoving(Tile _tile, Player _p, int x, int y) //Processes the tile if it was "moved" eg. It was clicked on to be moved to
        {
            if (_tile.Selectable) //If the tile can be moved to by the held piece
            {
                if (_chess.Map[_holdingX, _holdingY].GetPiece is Pawn) //If the held tile has a pawn
                {
                    (_chess.Map[_holdingX, _holdingY].GetPiece as Pawn).HasMoved = true; //Cast the piece as a pawn and change it's HasMoved attribute to true.
                }
                _chess.MovePiece(_holdingX, _holdingY, x, y);
                return true;
            }
            else //If not, cancel the move selection
            {
                return false;
            }
        }

        bool ProcessCapturing(Tile _tile, Player _p, int x, int y) //Processes the tile if it was "captured" eg. It was clicked on to be captured
        {
            if (_tile.Selectable && _tile != _chess.Map[_holdingX,_holdingY] && _tile.GetPiece.Colour != _chess.Map[_holdingX, _holdingY].GetPiece.Colour) //If the tile can be captured by the held piece
            {
                if (_chess.Map[_holdingX, _holdingY].GetPiece is Pawn) //If the held tile has a pawn
                {
                    (_chess.Map[_holdingX, _holdingY].GetPiece as Pawn).HasMoved = true; //Cast the piece as a pawn and change it's HasMoved attribute to true.
                }
                _chess.Map[x, y].GetPiece.IsAlive = false;
                _chess.MovePiece(_holdingX, _holdingY, x, y);
                return true;
            }
            else //If not, cancel the capture selection
            {
                return false;
            }
        }

        public Board Board
        {
            get
            {
                return _chess;
            }
        }

        public Player Player1
        {
            get
            {
                return _player1;
            }
        }

        public Player Player2
        {
            get
            {
                return _player2;
            }
        }

        public Player CurrentPlayer
        {
            get
            {
                return _currentPlayer;
            }
        }

        public bool ActivePlayer
        {
            get
            {
                return _activeplayer;
            }
        }

        public GameMode GameMode
        {
            set
            {
                _gm = value;
            }
        }
    }
}
