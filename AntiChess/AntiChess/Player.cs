﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AntiChess
{
    class Player
    {
        List<Piece> _pieces;

        public Player()
        {
            _pieces = new List<Piece>();
        }

        public bool IsFree(Tile[,] _chess) //Determines if the player is free to move (No pieces can capture)
        {
            int _xPos = -1, _yPos = -1;
            bool free = true;

            foreach (Piece p in _pieces) //For each of the player's pieces, search the board for it's coordinates and ask if it can capture
            {
                for (int xCoord = 0; xCoord < _chess.GetLength(0); xCoord++)
                {
                    for (int yCoord = 0; yCoord < _chess.GetLength(1); yCoord++)
                    {
                        if (_chess[xCoord, yCoord].GetPiece == p) //Finds the coordinates of the piece if it belongs to the player
                        {
                            _xPos = xCoord;
                            _yPos = yCoord;
                        }
                    }
                }

                if (p.IsAlive && p.CanCapture(_chess, _xPos, _yPos)) //If the piece at the coordinates are found, and it can capture
                {
                    free = false;
                }
            }

            return free; //A single return follows programming best practices
        }

        public void HighlightCapturable(Tile[,] _chess) //Highlights all of the pieces that can be captured by the player
        {
            int _xPos = -1, _yPos = -1;

            foreach (Piece p in _pieces) //Loops through all of the player's pieces, and highlight the ones that can capture
            {
                for (int xCoord = 0; xCoord < _chess.GetLength(0); xCoord++)
                {
                    for (int yCoord = 0; yCoord < _chess.GetLength(1); yCoord++)
                    {
                        if (_chess[xCoord, yCoord].GetPiece == p)
                        {
                            _xPos = xCoord;
                            _yPos = yCoord;
                        }
                    }
                }

                if (p.IsAlive && p.CanCapture(_chess, _xPos, _yPos))
                {
                    _chess[_xPos, _yPos].Selectable = true;
                }
            }

        }

        public int Score //Returns a count of all of the player's pieces that are still alive
        {
            get
            {
                int _count = 0;

                foreach (Piece _p in Pieces)
                {
                    if (_p.IsAlive)
                        _count++;
                }

                return _count;
            }
        }

        public List<Piece> Pieces
        {
            get
            {
                return _pieces;
            }
        }
    }
}
