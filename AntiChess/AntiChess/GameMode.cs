﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AntiChess
{
    enum GameMode
    {
        selecting,  //No pieces are selected, next piece would become active if belongs to the current player's turn
        moving,     //The selected piece is free to move, and the next clicked tile must be "selectable" for the piece to be moved
        capturing   //The selected piece can capture an opponent piece, the next clicked tile must be "selectable" for the piece to capture
    }
}