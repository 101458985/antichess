﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AntiChess
{
    class Tile
    {
        bool _active;
        bool _selectable;
        Color _colour;
        Piece _piece;

        public Tile(Piece piece, Color colour)
        {
            _colour = colour;
            _piece = piece;
        }

        public bool IsActive
        {
            get
            {
                return _active;
            }

            set
            {
                _active = value;    
            }
        }

        public bool Selectable
        {
            get
            {
                return _selectable;
            }

            set
            {
                _selectable = value;
            }
        }

        public Color Colour
        {
            get
            {
                return _colour;
            }

            set
            {
                _colour = value;
            }
        }

        public Piece GetPiece
        {
            get
            {
                return _piece;
            }

            set
            {
                _piece = value;
            }
        }
    }
}
