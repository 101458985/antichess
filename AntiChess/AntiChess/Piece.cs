﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace AntiChess
{
    abstract class Piece
    {
        bool _colour;
        bool _alive;
        string _name;
        Image _image;
        

        public Piece(bool colour, string name, Image image)
        {
            _colour = colour;
            _name = name;
            _image = image;
            _alive = true;
        }

        public abstract void HighlightMove(Board _chess, int x, int y);

        public abstract void HighlightCapture(Board _chess, int x, int y);

        public abstract bool CanCapture(Tile[,] _chess, int x, int y);

        public bool Colour
        {
            get
            {
                return _colour;
            }
        }

        public bool IsAlive
        {
            get
            {
                return _alive;
            }

            set
            {
                _alive = value;
            }
        }

        public string Name
        {
            get
            {
                return _name;
            }
        }

        public Image Icon
        {
            get
            {
                return _image;
            }
        }
    }
}
