﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AntiChess
{
    class Knight : Piece
    {
        public Knight(bool colour, Image icon) : base(colour, "Knight", icon)
        {
        }

        public override bool CanCapture(Tile[,] _chess, int x, int y)
        {
            bool _canCapture = false;

            //Try statements prevent out of bounds errors

            try //Checks upper left tile
            {
                if (_chess[x - 2, y - 1].GetPiece != null && _chess[x - 2, y - 1].GetPiece.Colour != Colour)
                    _canCapture = true;
            }
            catch { }

            try //Checks lower left tile
            {
                if (_chess[x - 2, y + 1].GetPiece != null && _chess[x - 2, y + 1].GetPiece.Colour != Colour)
                    _canCapture = true;
            }
            catch { }

            try //Checks upper right tile
            {
                if (_chess[x + 2, y - 1].GetPiece != null && _chess[x + 2, y - 1].GetPiece.Colour != Colour)
                    _canCapture = true;
            }
            catch { }

            try //Checks lower right tile
            {
                if (_chess[x + 2, y + 1].GetPiece != null && _chess[x + 2, y + 1].GetPiece.Colour != Colour)
                    _canCapture = true;
            }
            catch { }

            try //Checks left upwards tile
            {
                if (_chess[x - 1, y - 2].GetPiece != null && _chess[x - 1, y - 2].GetPiece.Colour != Colour)
                    _canCapture = true;
            }
            catch { }

            try //Checks right upwards tile
            {
                if (_chess[x + 1, y - 2].GetPiece != null && _chess[x + 1, y - 2].GetPiece.Colour != Colour)
                    _canCapture = true;
            }
            catch { }

            try //Checks left downwards tile
            {
                if (_chess[x - 1, y + 2].GetPiece != null && _chess[x - 1, y + 2].GetPiece.Colour != Colour)
                    _canCapture = true;
            }
            catch { }

            try //Checks right downwards tile
            {
                if (_chess[x + 1, y + 2].GetPiece != null && _chess[x + 1, y + 2].GetPiece.Colour != Colour)
                    _canCapture = true;
            }
            catch { }

            return _canCapture;
        }

        public override void HighlightMove(Board _chess, int x, int y)
        {
            try //Highlights upper left tile
            {
                if (_chess.Map[x - 2, y - 1].GetPiece == null)
                    _chess.Map[x - 2, y - 1].Selectable = true;
            }
            catch { }

            try //Highlights lower left tile
            {
                if (_chess.Map[x - 2, y + 1].GetPiece == null)
                    _chess.Map[x - 2, y + 1].Selectable = true;
            }
            catch { }

            try //Highlights upper right tile
            {
                if (_chess.Map[x + 2, y - 1].GetPiece == null)
                    _chess.Map[x + 2, y - 1].Selectable = true;
            }
            catch { }

            try //Highlights lower right tile
            {
                if (_chess.Map[x + 2, y + 1].GetPiece == null)
                    _chess.Map[x + 2, y + 1].Selectable = true;
            }
            catch { }

            try //Highlights left upwards tile
            {
                if (_chess.Map[x - 1, y - 2].GetPiece == null)
                    _chess.Map[x - 1, y - 2].Selectable = true;
            }
            catch { }

            try //Highlights right upwards tile
            {
                if (_chess.Map[x + 1, y - 2].GetPiece == null)
                    _chess.Map[x + 1, y - 2].Selectable = true;
            }
            catch { }

            try //Highlights left downwards tile
            {
                if (_chess.Map[x - 1, y + 2].GetPiece == null)
                    _chess.Map[x - 1, y + 2].Selectable = true;
            }
            catch { }

            try //Highlights right downwards tile
            {
                if (_chess.Map[x + 1, y + 2].GetPiece == null)
                    _chess.Map[x + 1, y + 2].Selectable = true;
            }
            catch { }
        }

        public override void HighlightCapture(Board _chess, int x, int y)
        {
            try //Checks upper left tile
            {
                if (_chess.Map[x - 2, y - 1].GetPiece != null && _chess.Map[x - 2, y - 1].GetPiece.Colour != Colour)
                    _chess.Map[x - 2, y - 1].Selectable = true;
            }
            catch { }

            try //Checks lower left tile
            {
                if (_chess.Map[x - 2, y + 1].GetPiece != null && _chess.Map[x - 2, y + 1].GetPiece.Colour != Colour)
                    _chess.Map[x - 2, y + 1].Selectable = true;
            }
            catch { }

            try //Checks upper right tile
            {
                if (_chess.Map[x + 2, y - 1].GetPiece != null && _chess.Map[x + 2, y - 1].GetPiece.Colour != Colour)
                    _chess.Map[x + 2, y - 1].Selectable = true;
            }
            catch { }

            try //Checks lower right tile
            {
                if (_chess.Map[x + 2, y + 1].GetPiece != null && _chess.Map[x + 2, y + 1].GetPiece.Colour != Colour)
                    _chess.Map[x + 2, y + 1].Selectable = true;
            }
            catch { }

            try //Checks left upwards tile
            {
                if (_chess.Map[x - 1, y - 2].GetPiece != null && _chess.Map[x - 1, y - 2].GetPiece.Colour != Colour)
                    _chess.Map[x - 1, y - 2].Selectable = true;
            }
            catch { }

            try //Checks right upwards tile
            {
                if (_chess.Map[x + 1, y - 2].GetPiece != null && _chess.Map[x + 1, y - 2].GetPiece.Colour != Colour)
                    _chess.Map[x + 1, y - 2].Selectable = true;
            }
            catch { }

            try //Checks left downwards tile
            {
                if (_chess.Map[x - 1, y + 2].GetPiece != null && _chess.Map[x - 1, y + 2].GetPiece.Colour != Colour)
                    _chess.Map[x - 1, y + 2].Selectable = true;
            }
            catch { }

            try //Checks right downwards tile
            {
                if (_chess.Map[x + 1, y + 2].GetPiece != null && _chess.Map[x + 1, y + 2].GetPiece.Colour != Colour)
                    _chess.Map[x + 1, y + 2].Selectable = true;
            }
            catch { }
        }
    }
}
