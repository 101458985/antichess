﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AntiChess
{
    public partial class GameScreen : Form
    {
        Game _game;
        Color[] _colours;

        public GameScreen()
        {
            _game = new Game();
            _colours = new Color[] {Color.DarkGoldenrod, Color.Moccasin, Color.PaleGreen, Color.LimeGreen }; //Default colours of the board
            InitializeComponent();
            InitGame();
        }

        void InitGame()
        {

            for (int _xPos = 0; _xPos <= 7; _xPos++) //Creates new tile objects for the 8x8 array
            {
                for (int _yPos = 0; _yPos <= 7; _yPos++)
                {
                    _game.Board.Map[_xPos, _yPos] = new Tile(null, Color.Black);
                }
            }

            AssignColours();

            OutputBox.Text = "White to move"; //Manually write the initial values in the text boxes. Most efficient method in the context of the program
            WhiteScore.Text = "16";
            BlackScore.Text = "16";

            _game.Board.Map[0, 0].GetPiece = new Rook(true, Properties.Resources.BlackRook); //Creates all pieces and assigns them to the appropriate tiles
            _game.Board.Map[1, 0].GetPiece = new Knight(true, Properties.Resources.BlackKnight);
            _game.Board.Map[2, 0].GetPiece = new Bishop(true, Properties.Resources.BlackBishop);
            _game.Board.Map[3, 0].GetPiece = new King(true, Properties.Resources.BlackKing);
            _game.Board.Map[4, 0].GetPiece = new Queen(true, Properties.Resources.BlackQueen);
            _game.Board.Map[5, 0].GetPiece = new Bishop(true, Properties.Resources.BlackBishop);
            _game.Board.Map[6, 0].GetPiece = new Knight(true, Properties.Resources.BlackKnight);
            _game.Board.Map[7, 0].GetPiece = new Rook(true, Properties.Resources.BlackRook);

            _game.Board.Map[0, 1].GetPiece = new Pawn(true, Properties.Resources.BlackPawn);
            _game.Board.Map[1, 1].GetPiece = new Pawn(true, Properties.Resources.BlackPawn);
            _game.Board.Map[2, 1].GetPiece = new Pawn(true, Properties.Resources.BlackPawn);
            _game.Board.Map[3, 1].GetPiece = new Pawn(true, Properties.Resources.BlackPawn);
            _game.Board.Map[4, 1].GetPiece = new Pawn(true, Properties.Resources.BlackPawn);
            _game.Board.Map[5, 1].GetPiece = new Pawn(true, Properties.Resources.BlackPawn);
            _game.Board.Map[6, 1].GetPiece = new Pawn(true, Properties.Resources.BlackPawn);
            _game.Board.Map[7, 1].GetPiece = new Pawn(true, Properties.Resources.BlackPawn);

            _game.Board.Map[0, 7].GetPiece = new Rook(false, Properties.Resources.WhiteRook);
            _game.Board.Map[1, 7].GetPiece = new Knight(false, Properties.Resources.WhiteKnight);
            _game.Board.Map[2, 7].GetPiece = new Bishop(false, Properties.Resources.WhiteBishop);
            _game.Board.Map[3, 7].GetPiece = new King(false, Properties.Resources.WhiteKing);
            _game.Board.Map[4, 7].GetPiece = new Queen(false, Properties.Resources.WhiteQueen);
            _game.Board.Map[5, 7].GetPiece = new Bishop(false, Properties.Resources.WhiteBishop);
            _game.Board.Map[6, 7].GetPiece = new Knight(false, Properties.Resources.WhiteKnight);
            _game.Board.Map[7, 7].GetPiece = new Rook(false, Properties.Resources.WhiteRook);

            _game.Board.Map[0, 6].GetPiece = new Pawn(false, Properties.Resources.WhitePawn);
            _game.Board.Map[1, 6].GetPiece = new Pawn(false, Properties.Resources.WhitePawn);
            _game.Board.Map[2, 6].GetPiece = new Pawn(false, Properties.Resources.WhitePawn);
            _game.Board.Map[3, 6].GetPiece = new Pawn(false, Properties.Resources.WhitePawn);
            _game.Board.Map[4, 6].GetPiece = new Pawn(false, Properties.Resources.WhitePawn);
            _game.Board.Map[5, 6].GetPiece = new Pawn(false, Properties.Resources.WhitePawn);
            _game.Board.Map[6, 6].GetPiece = new Pawn(false, Properties.Resources.WhitePawn);
            _game.Board.Map[7, 6].GetPiece = new Pawn(false, Properties.Resources.WhitePawn);

            _game.Player1.Pieces.Add(_game.Board.Map[0, 7].GetPiece); //Assigns all pieces to players
            _game.Player1.Pieces.Add(_game.Board.Map[1, 7].GetPiece);
            _game.Player1.Pieces.Add(_game.Board.Map[2, 7].GetPiece);
            _game.Player1.Pieces.Add(_game.Board.Map[3, 7].GetPiece);
            _game.Player1.Pieces.Add(_game.Board.Map[4, 7].GetPiece);
            _game.Player1.Pieces.Add(_game.Board.Map[5, 7].GetPiece);
            _game.Player1.Pieces.Add(_game.Board.Map[6, 7].GetPiece);
            _game.Player1.Pieces.Add(_game.Board.Map[7, 7].GetPiece);
            _game.Player1.Pieces.Add(_game.Board.Map[0, 6].GetPiece);
            _game.Player1.Pieces.Add(_game.Board.Map[1, 6].GetPiece);
            _game.Player1.Pieces.Add(_game.Board.Map[2, 6].GetPiece);
            _game.Player1.Pieces.Add(_game.Board.Map[3, 6].GetPiece);
            _game.Player1.Pieces.Add(_game.Board.Map[4, 6].GetPiece);
            _game.Player1.Pieces.Add(_game.Board.Map[5, 6].GetPiece);
            _game.Player1.Pieces.Add(_game.Board.Map[6, 6].GetPiece);
            _game.Player1.Pieces.Add(_game.Board.Map[7, 6].GetPiece);

            _game.Player2.Pieces.Add(_game.Board.Map[0, 0].GetPiece);
            _game.Player2.Pieces.Add(_game.Board.Map[1, 0].GetPiece);
            _game.Player2.Pieces.Add(_game.Board.Map[2, 0].GetPiece);
            _game.Player2.Pieces.Add(_game.Board.Map[3, 0].GetPiece);
            _game.Player2.Pieces.Add(_game.Board.Map[4, 0].GetPiece);
            _game.Player2.Pieces.Add(_game.Board.Map[5, 0].GetPiece);
            _game.Player2.Pieces.Add(_game.Board.Map[6, 0].GetPiece);
            _game.Player2.Pieces.Add(_game.Board.Map[7, 0].GetPiece);
            _game.Player2.Pieces.Add(_game.Board.Map[0, 1].GetPiece);
            _game.Player2.Pieces.Add(_game.Board.Map[1, 1].GetPiece);
            _game.Player2.Pieces.Add(_game.Board.Map[2, 1].GetPiece);
            _game.Player2.Pieces.Add(_game.Board.Map[3, 1].GetPiece);
            _game.Player2.Pieces.Add(_game.Board.Map[4, 1].GetPiece);
            _game.Player2.Pieces.Add(_game.Board.Map[5, 1].GetPiece);
            _game.Player2.Pieces.Add(_game.Board.Map[6, 1].GetPiece);
            _game.Player2.Pieces.Add(_game.Board.Map[7, 1].GetPiece);
            
            CalculateOdds();
            RenderAllTiles();
        }

        public void RestartGame() //Reinitalises the game to be played again
        {
            _game = new Game();
            InitGame();
        }

        void AssignColours() //Assigns the colours to all tiles
        {
            bool _darkTile = true; //Boolean determines what colour is the tile
            Color _tileColour;

            for (int _xPos = 0; _xPos <= 7; _xPos++)
            {
                for (int _yPos = 0; _yPos <= 7; _yPos++)
                {
                    if (_darkTile)
                        _tileColour = _colours[0];
                    else
                        _tileColour = _colours[1];

                    _game.Board.Map[_xPos, _yPos].Colour = _tileColour;

                    _darkTile = !_darkTile; //Inverts tile colour for next column
                }
                _darkTile = !_darkTile; //Inverts tile colour for next row
            }

        }

        void checkBox1_CheckedChanged(object sender, EventArgs e) //Toggles colourblind mode
        {
            if (checkBox1.Checked) //If colorblind mode is checked, change the colours in the array to a colorblind-friendly scheme
            {
                _colours[0] = Color.DimGray;
                _colours[1] = Color.DarkGray;
                _colours[2] = Color.WhiteSmoke;
                _colours[3] = Color.SlateGray;
            }
            else
            {
                _colours[0] = Color.DarkGoldenrod;
                _colours[1] = Color.Moccasin;
                _colours[2] = Color.PaleGreen;
                _colours[3] = Color.LimeGreen;
            }

            AssignColours();
            RenderAllTiles();
        }

        void RenderTile(Panel panel, Tile _tile) //Renders a panel onto the screen
        {
            if (_tile.IsActive) //Determines the colour of the tile
                panel.BackColor = _colours[2];
            else if (_tile.Selectable)
                panel.BackColor = _colours[3];
            else
                panel.BackColor = _tile.Colour;

            if (_tile.GetPiece != null) //Draws the piece onto the tile, if any
                panel.BackgroundImage = _tile.GetPiece.Icon;
            else
                panel.BackgroundImage = null;
        }

        void CalculateOdds() //Calculates the odds display outputs by performing basic arithmetic on the player score.
        {
            float _totalScore;
            float _whiteOdds;
            float _blackOdds;

            _totalScore = _game.Player1.Score + _game.Player2.Score;
            _whiteOdds = _game.Player2.Score / _totalScore * 100;
            _blackOdds = _game.Player1.Score / _totalScore * 100;

            WhiteOdds.Text = _whiteOdds.ToString("##.#");
            BlackOdds.Text = _blackOdds.ToString("##.#");
        }

        void DoTurn(Tile _tile, int x, int y) //Happens every time a tile is clicked on the board, processes the "action", aka "turn"
        {
            string _toPrint;
            string _currentColour;

            _game.ProcessTile(_tile, x, y);
            RenderAllTiles();

            if (_game.ActivePlayer) //Detemines the string to be output on the text box depending on the active player
                _currentColour = "Black";
            else
                _currentColour = "White";

            _toPrint = _currentColour + " to move\r\n";

            if (!_game.CurrentPlayer.IsFree(_game.Board.Map)) //Determines whether or not to print "must capture" depending on the active player's IsFree flag
                _toPrint += _currentColour + " must capture";

            WhiteScore.Text = _game.Player1.Score.ToString();
            BlackScore.Text = _game.Player2.Score.ToString();

            OutputBox.Text = _toPrint;
            CalculateOdds();

            CheckVictory();
        }

        void CheckVictory() //Checks if a player has won. Called every turn
        {
            if (_game.Player1.Score == 0) //If player 1 has reached the score of zero, player 1 wins
            {
                EndScreen _endScreen = new EndScreen("White", "elimination", _game.Player1, _game.Player2);
                _endScreen.Tag = this; //Opens the next form and hides this one
                _endScreen.Show(this);
                _endScreen.Location = new Point(this.Left, this.Top);
                Hide();
            }
            else if (_game.Player2.Score == 0)
            {
                EndScreen _endScreen = new EndScreen("Black", "elimination", _game.Player1, _game.Player2);
                _endScreen.Tag = this;
                _endScreen.Show(this);
                _endScreen.Location = new Point(this.Left, this.Top);
                Hide();
            }
        }

        void ConcedeGame(bool _winner) //Called when a player concedes
        {
            string winner;
            if (_winner) //Determines the string of the player
                winner = "Black";
            else
                winner = "White";

            EndScreen _endScreen = new EndScreen(winner, "forfeiture", _game.Player1, _game.Player2); //Opens the game over screen and hides this one
            _endScreen.Tag = this;
            _endScreen.Show(this);
            _endScreen.Location = new Point(this.Left, this.Top);
            Hide();

        }

        void RenderAllTiles() //A loop was not able to be implemented as each panel must be referred to individually by name
        {

            RenderTile(tile1a, _game.Board.Map[0, 0]);
            RenderTile(tile1b, _game.Board.Map[1, 0]);
            RenderTile(tile1c, _game.Board.Map[2, 0]);
            RenderTile(tile1d, _game.Board.Map[3, 0]);
            RenderTile(tile1e, _game.Board.Map[4, 0]);
            RenderTile(tile1f, _game.Board.Map[5, 0]);
            RenderTile(tile1g, _game.Board.Map[6, 0]);
            RenderTile(tile1h, _game.Board.Map[7, 0]);
            RenderTile(tile2a, _game.Board.Map[0, 1]);
            RenderTile(tile2b, _game.Board.Map[1, 1]);
            RenderTile(tile2c, _game.Board.Map[2, 1]);
            RenderTile(tile2d, _game.Board.Map[3, 1]);
            RenderTile(tile2e, _game.Board.Map[4, 1]);
            RenderTile(tile2f, _game.Board.Map[5, 1]);
            RenderTile(tile2g, _game.Board.Map[6, 1]);
            RenderTile(tile2h, _game.Board.Map[7, 1]);
            RenderTile(tile3a, _game.Board.Map[0, 2]);
            RenderTile(tile3b, _game.Board.Map[1, 2]);
            RenderTile(tile3c, _game.Board.Map[2, 2]);
            RenderTile(tile3d, _game.Board.Map[3, 2]);
            RenderTile(tile3e, _game.Board.Map[4, 2]);
            RenderTile(tile3f, _game.Board.Map[5, 2]);
            RenderTile(tile3g, _game.Board.Map[6, 2]);
            RenderTile(tile3h, _game.Board.Map[7, 2]);
            RenderTile(tile4a, _game.Board.Map[0, 3]);
            RenderTile(tile4b, _game.Board.Map[1, 3]);
            RenderTile(tile4c, _game.Board.Map[2, 3]);
            RenderTile(tile4d, _game.Board.Map[3, 3]);
            RenderTile(tile4e, _game.Board.Map[4, 3]);
            RenderTile(tile4f, _game.Board.Map[5, 3]);
            RenderTile(tile4g, _game.Board.Map[6, 3]);
            RenderTile(tile4h, _game.Board.Map[7, 3]);
            RenderTile(tile5a, _game.Board.Map[0, 4]);
            RenderTile(tile5b, _game.Board.Map[1, 4]);
            RenderTile(tile5c, _game.Board.Map[2, 4]);
            RenderTile(tile5d, _game.Board.Map[3, 4]);
            RenderTile(tile5e, _game.Board.Map[4, 4]);
            RenderTile(tile5f, _game.Board.Map[5, 4]);
            RenderTile(tile5g, _game.Board.Map[6, 4]);
            RenderTile(tile5h, _game.Board.Map[7, 4]);
            RenderTile(tile6a, _game.Board.Map[0, 5]);
            RenderTile(tile6b, _game.Board.Map[1, 5]);
            RenderTile(tile6c, _game.Board.Map[2, 5]);
            RenderTile(tile6d, _game.Board.Map[3, 5]);
            RenderTile(tile6e, _game.Board.Map[4, 5]);
            RenderTile(tile6f, _game.Board.Map[5, 5]);
            RenderTile(tile6g, _game.Board.Map[6, 5]);
            RenderTile(tile6h, _game.Board.Map[7, 5]);
            RenderTile(tile7a, _game.Board.Map[0, 6]);
            RenderTile(tile7b, _game.Board.Map[1, 6]);
            RenderTile(tile7c, _game.Board.Map[2, 6]);
            RenderTile(tile7d, _game.Board.Map[3, 6]);
            RenderTile(tile7e, _game.Board.Map[4, 6]);
            RenderTile(tile7f, _game.Board.Map[5, 6]);
            RenderTile(tile7g, _game.Board.Map[6, 6]);
            RenderTile(tile7h, _game.Board.Map[7, 6]);
            RenderTile(tile8a, _game.Board.Map[0, 7]);
            RenderTile(tile8b, _game.Board.Map[1, 7]);
            RenderTile(tile8c, _game.Board.Map[2, 7]);
            RenderTile(tile8d, _game.Board.Map[3, 7]);
            RenderTile(tile8e, _game.Board.Map[4, 7]);
            RenderTile(tile8f, _game.Board.Map[5, 7]);
            RenderTile(tile8g, _game.Board.Map[6, 7]);
            RenderTile(tile8h, _game.Board.Map[7, 7]);
        }

        void tile1a_Click(object sender, EventArgs e) //Each of the 64 panels on the board will process their individual tile from the array
        {
            Tile _tile = _game.Board.Map[0, 0];
            DoTurn(_tile, 0, 0);
        }

        void tile1b_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[1, 0];
            DoTurn(_tile, 1, 0);
        }

        void tile1c_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[2, 0];
            DoTurn(_tile, 2, 0);
        }

        void tile1d_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[3, 0];
            DoTurn(_tile, 3, 0);
        }

        void tile1e_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[4, 0];
            DoTurn(_tile, 4, 0);
        }

        void tile1f_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[5, 0];
            DoTurn(_tile, 5, 0);
        }

        void tile1g_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[6, 0];
            DoTurn(_tile, 6, 0);
        }

        void tile1h_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[7, 0];
            DoTurn(_tile, 7, 0);
        }

        void tile2a_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[0, 1];
            DoTurn(_tile, 0 ,1);
        }

        void tile2b_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[1, 1];
            DoTurn(_tile, 1, 1);
        }

        void tile2c_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[2, 1];
            DoTurn(_tile, 2, 1);
        }

        void tile2d_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[3, 1];
            DoTurn(_tile, 3, 1);        }

        void tile2e_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[4, 1];
            DoTurn(_tile, 4, 1);
        }

        void tile2f_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[5, 1];
            DoTurn(_tile, 5, 1);
        }

        void tile2g_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[6, 1];
            DoTurn(_tile, 6, 1);
        }

        void tile2h_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[7, 1];
            DoTurn(_tile, 7, 1);
        }

        void tile3a_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[0, 2];
            DoTurn(_tile, 0, 2);
        }

        void tile3b_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[1, 2];
            DoTurn(_tile, 1, 2);
        }

        void tile3c_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[2, 2];
            DoTurn(_tile, 2, 2);
        }

        void tile3d_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[3, 2];
            DoTurn(_tile, 3, 2);
        }

        void tile3e_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[4, 2];
            DoTurn(_tile, 4, 2);
        }

        void tile3f_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[5, 2];
            DoTurn(_tile, 5, 2);
        }

        void tile3g_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[6, 2];
            DoTurn(_tile, 6, 2);
        }

        void tile3h_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[7, 2];
            DoTurn(_tile, 7, 2);
        }

        void tile4a_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[0, 3];
            DoTurn(_tile, 0, 3);
        }

        void tile4b_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[1, 3];
            DoTurn(_tile, 1, 3);
        }

        void tile4c_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[2, 3];
            DoTurn(_tile, 2, 3);
        }

        void tile4d_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[3, 3];
            DoTurn(_tile, 3, 3);        }

        void tile4e_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[4, 3];
            DoTurn(_tile, 4, 3);
        }

        void tile4f_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[5, 3];
            DoTurn(_tile, 5, 3);
        }

        void tile4g_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[6, 3];
            DoTurn(_tile, 6, 3);
        }

        void tile4h_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[7, 3];
            DoTurn(_tile, 7, 3);
        }

        void tile5a_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[0, 4];
            DoTurn(_tile, 0, 4);        }

        void tile5b_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[1, 4];
            DoTurn(_tile, 1, 4);
        }

        void tile5c_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[2, 4];
            DoTurn(_tile, 2, 4);
        }

        void tile5d_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[3, 4];
            DoTurn(_tile, 3, 4);
        }

        void tile5e_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[4, 4];
            DoTurn(_tile, 4, 4);
        }

        void tile5f_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[5, 4];
            DoTurn(_tile, 5, 4);
        }

        void tile5g_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[6, 4];
            DoTurn(_tile, 6, 4);
        }

        void tile5h_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[7, 4];
            DoTurn(_tile, 7, 4);
        }

        void tile6a_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[0, 5];
            DoTurn(_tile, 0, 5);
        }

        void tile6b_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[1, 5];
            DoTurn(_tile, 1, 5);
        }

        void tile6c_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[2, 5];
            DoTurn(_tile, 2, 5);
        }

        void tile6d_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[3, 5];
            DoTurn(_tile, 3, 5);
        }

        void tile6e_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[4, 5];
            DoTurn(_tile, 4, 5);
        }

        void tile6f_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[5, 5];
            DoTurn(_tile, 5, 5);
        }

        void tile6g_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[6, 5];
            DoTurn(_tile, 6, 5);
        }

        void tile6h_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[7, 5];
            DoTurn(_tile, 7, 5);
        }

        void tile7a_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[0, 6];
            DoTurn(_tile, 0, 6);
        }

        void tile7b_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[1, 6];
            DoTurn(_tile, 1, 6);
        }

        void tile7c_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[2, 6];
            DoTurn(_tile,2, 6);
        }

        void tile7d_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[3, 6];
            DoTurn(_tile, 3, 6);
        }

        void tile7e_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[4, 6];
            DoTurn(_tile, 4, 6);
        }

        void tile7f_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[5, 6];
            DoTurn(_tile, 5, 6);
        }

        void tile7g_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[6, 6];
            DoTurn(_tile, 6, 6);
        }

        void tile7h_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[7, 6];
            DoTurn(_tile, 7, 6);
        }

        void tile8a_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[0, 7];
            DoTurn(_tile, 0, 7);
        }

        void tile8b_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[1, 7];
            DoTurn(_tile, 1, 7);
        }

        void tile8c_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[2, 7];
            DoTurn(_tile, 2, 7);
        }

        void tile8d_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[3, 7];
            DoTurn(_tile, 3, 7);
        }

        void tile8e_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[4, 7];
            DoTurn(_tile, 4, 7);
        }

        void tile8f_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[5, 7];
            DoTurn(_tile, 5, 7);
        }

        void tile8g_Click(object sender, EventArgs e)
        {
            Tile _tile = _game.Board.Map[6, 7];
            DoTurn(_tile, 6, 7);
        }

        void tile8h_Click(object sender, EventArgs e)
        {

            Tile _tile = _game.Board.Map[7, 7];
            DoTurn(_tile, 7, 7);
        }

        void CancelButton_Click(object sender, EventArgs e)
        {
            Tile _placeholder = null;
            _game.ResetAllTiles();
            _game.ProcessTile(_placeholder, -1, -1);    //Cancelling resets all tiles, and a ProcessTile method must be run to re-update the tiles.
                                                        //This is required due to the design and structure of the methods. A fix to this would require a major re-write of the program
            _game.GameMode = GameMode.selecting;
            RenderAllTiles();
        }

        void ConcedeButton_Click(object sender, EventArgs e)
        {
            ConcedeGame(!_game.ActivePlayer);
        }
    }
}
