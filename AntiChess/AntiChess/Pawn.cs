﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Resources;
using System.Threading.Tasks;

namespace AntiChess
{
    class Pawn : Piece
    {
        bool _hasMoved; //Pawns can move 2 spaces for their first move

        public Pawn(bool colour, Image icon) : base(colour, "Pawn", icon)
        {
            _hasMoved = false;
        }

        public override bool CanCapture(Tile[,] _chess, int x, int y)
        {
            bool _canCapture = false;
            if (Colour) //If piece is black
            {
                try //Tries prevent out of bounds errors
                {
                    if (_chess[x - 1, y + 1].GetPiece != null && !_chess[x - 1, y + 1].GetPiece.Colour) //If it is not empty and it is white
                        _canCapture = true;
                }
                catch { }

                try
                {
                    if (_chess[x + 1, y + 1].GetPiece != null && !_chess[x + 1, y + 1].GetPiece.Colour)
                        _canCapture = true;
                }
                catch { }
            }
            else //If piece is white
            {
                try
                {
                    if (_chess[x - 1, y - 1].GetPiece != null && _chess[x - 1, y - 1].GetPiece.Colour) //If it is not empty and it is black
                        _canCapture = true;
                }
                catch { }

                try
                {
                    if (_chess[x + 1, y - 1].GetPiece != null && _chess[x + 1, y - 1].GetPiece.Colour)
                        _canCapture = true;
                }
                catch { }

            }

            return _canCapture;
        }

        public override void HighlightMove(Board _chess, int x, int y)
        {
            if (Colour) //If piece is black
            {
                if (!_hasMoved) //If the pawn hasn't moved, it can move 2 spaces
                {
                    if (_chess.Map[x, y + 1].GetPiece == null) //If the tile in front is empty, then it can be moved to
                    {
                        _chess.Map[x, y + 1].Selectable = true;

                        if (_chess.Map[x, y + 2].GetPiece == null) //If the first tile in front is empty, then check if the second tile is empty too
                            _chess.Map[x, y + 2].Selectable = true;

                    }
                }
                else
                    try
                    {
                        if (_chess.Map[x, y + 1].GetPiece == null)
                            _chess.Map[x, y + 1].Selectable = true;
                    }
                    catch { }
            }
            else //If piece is white
            {
                if (!_hasMoved)
                {
                    if (_chess.Map[x, y - 1].GetPiece == null)
                    {
                        _chess.Map[x, y - 1].Selectable = true;

                        if (_chess.Map[x, y - 2].GetPiece == null)
                            _chess.Map[x, y - 2].Selectable = true;
                    }
                }
                else
                    try
                    {
                        if (_chess.Map[x, y - 1].GetPiece == null)
                            _chess.Map[x, y - 1].Selectable = true;
                    }
                    catch { }
            }
        }

        public override void HighlightCapture(Board _chess, int x, int y)
        {
            if (Colour) //If piece is black
            {
                try //Tries prevent out of bounds errors
                {
                    if (_chess.Map[x - 1, y + 1].GetPiece != null && !_chess.Map[x - 1, y + 1].GetPiece.Colour) //If it is not empty and it is white
                        _chess.Map[x - 1, y + 1].Selectable = true;
                }
                catch { }

                try
                {
                    if (_chess.Map[x + 1, y + 1].GetPiece != null && !_chess.Map[x + 1, y + 1].GetPiece.Colour)
                        _chess.Map[x + 1, y + 1].Selectable = true;
                }
                catch { }
            }
            else //If piece is white
            {
                try
                {
                    if (_chess.Map[x - 1, y - 1].GetPiece != null && _chess.Map[x - 1, y - 1].GetPiece.Colour) //If it is not empty and it is black
                        _chess.Map[x - 1, y - 1].Selectable = true;
                }
                catch { }

                try
                {
                    if (_chess.Map[x + 1, y - 1].GetPiece != null && _chess.Map[x + 1, y - 1].GetPiece.Colour)
                        _chess.Map[x + 1, y - 1].Selectable = true;
                }
                catch { }
            }
        }

        public bool HasMoved
        {
            set
            {
                _hasMoved = value;
            }
        }
    }
}
