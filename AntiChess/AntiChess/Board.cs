﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Threading.Tasks;

namespace AntiChess
{
    class Board
    {
        Tile[,] _map;

        public Board()
        {
            _map = new Tile[8,8];
        }

        public void MovePiece(int x1, int y1, int x2, int y2)
        {
            _map[x2, y2].GetPiece = _map[x1, y1].GetPiece;
            _map[x1, y1].GetPiece = null;
        }

        public void SelectTile(int x, int y)
        {
            _map[x, y].IsActive = true;
        }

        public Tile[,] Map
        {
            get
            {
                return _map;
            }

            set
            {
                _map = value;
            }
        }
    }
}
